package br.jus.stf.seguranca.icp.service.test;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.domain.AssinaturaDigital;
import br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora;
import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.RaizNaoEncontradaException;
import br.jus.stf.seguranca.icp.service.impl.IcpServiceImpl;

@SuppressWarnings({ "restriction", "deprecation" })
@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
public class TestCadastroService {
	
	private CadastroCertificadoService cadastroService;
	private IcpServiceImpl icpService;
	
	@Test
	public void testConfig() {
		Assert.assertNotNull(cadastroService);
	}
	
	@Test
	public void testAll() throws CertificateException, CadeiaException, IOException, CertificadoInvalidoException, RaizNaoEncontradaException, CRLInvalidaException {
		List<AssinaturaDigital> usuarios = cadastroService.findAllAssinaturaDigital();
		Assert.assertTrue(usuarios.size() > 0);
		
		List<AutoridadeCertificadora> autoridadesCertificadoras = cadastroService.findAllAutoridadeCertificadora();
		Assert.assertTrue(autoridadesCertificadoras.size() > 0);
		AutoridadeCertificadora ac = autoridadesCertificadoras.get(0);
		X509Certificate cert = ac.getX509();
		
		Assert.assertNotNull(cadastroService.findAutoridadeCertificadora(cert));
		Assert.assertNotNull(cadastroService.findCertificadoDigital(cert));
		Assert.assertEquals(cadastroService.findCertificadoDigital(cert.getSubjectX500Principal().getName()).size(),1);
		List<X509Certificate> chain = cadastroService.findChain(cert);
		Assert.assertTrue(chain.size() > 0);
		CertificadoDigital certDigital = cadastroService.findCertificadoDigital(chain.get(1));
		Assert.assertTrue(cadastroService.findLatestCrl(certDigital)!=null);
		
		X509Certificate certUsuario = usuarios.get(0).getX509();
		icpService.validarCertificado(new X509Certificate[]{certUsuario}, certUsuario.getNotAfter(), false);
	}

	public CadastroCertificadoService getCadastroService() {
		return cadastroService;
	}

	@Resource(name="cadastroService")
	public void setCadastroService(CadastroCertificadoService cadastroService) {
		this.cadastroService = cadastroService;
	}
	
	public IcpService getIcpService() {
		return icpService;
	}

	@Resource(name="icpService")
	public void setIcpService(IcpServiceImpl icpService) {
		this.icpService = icpService;
	}
}
