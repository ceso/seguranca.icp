package br.jus.stf.seguranca.icp.service.test;

import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bouncycastle.asn1.ASN1InputStream;
//import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.X509Extensions;

public class DetalheCert {
	// Campo do OtherName do Subject Alternative Name
	private static final int TYPE_OTHERNAME = 0;

	// Campo do OtherName do Subject Alternative Name
	private static final int TYPE_RFC822_NAME = 1;
	static String[] keyUsageNames = 
		     {"digitalSignature",
		     "nonRepudiation",
		     "keyEncipherment",
		     "dataEncipherment",
		     "keyAgreement",
		     "keyCertSign",
		     "cRLSign",
		     "encipherOnly",
		     "decipherOnly"};
	
	static String[] nameTypes = 
			{"otherName",
		    "rfc822Name",
		    "dNSName",
		    "x400Address",
		    "directoryName",
		    "ediPartyName",
		    "uniformResourceIdentifier",
		    "iPAddress",
		    "registeredID"};
	
	static String[][] oidToString = {
		{"1.3.6.1.5.5.7.3.2", "id_kp_clientAuth"},
		{"1.3.6.1.5.5.7.3.4","id_kp_emailProtection"},
		{"1.3.6.1.4.1.311.20.2.2","Microsoft Smart Card Logon"},
		{"1.3.6.1.5.5.7.3.1","id_kp_serverAuth"},
		{"1.3.6.1.5.5.7.3.3","id_kp_codeSigning"},
		{"1.3.6.1.5.5.7.3.8","id_kp_timeStamping"},
		{"2.5.29.15","Key Usage"},
		{"2.5.29.37","Extended key usage"},
		{"2.5.29.19","Basic Constraints"},
		{"2.5.29.15","Key Usage"},
		{"2.5.29.37","Extended key usage"},
		{"2.5.29.19","Basic Constraints"},
		{"2.16.76.1.3.1","ICP-Brasil Dados de Pessoa Fisica"},
		{"2.16.76.1.3.5","ICP-Brasil - Titulo de Eleitor - Pessoa Fisica"},
		{"2.16.76.1.3.6","INSS"},
		{"1.3.6.1.4.1.311.20.2.3","NT_PRINCIPAL_NAME"},
		{"2.16.76.1.4.2.1.1","NÃºmero de registro do advogado"},
		{"2.16.76.1.3.4","ICP-Brasil - Dados do Responsavel - Pessoa Juridica"},
		{"2.16.76.1.3.2","ICP-Brasil - Nome do Responsavel - Pessoa Juridica"},
		{"2.16.76.1.3.3","ICP-Brasil - CNPJ - Pessoa Juridica"},
		{"2.16.76.1.3.7","Cadastro EspecÃ­fico do INSS (CEI) da pessoa jurÃ­dica"}
	};
	
	
	String sigAlg;
	String sigOid;
	byte[] sigAlgParams;
	boolean[] keyUsage;
	List<String> extendedKeyUsage;
	Set<String> criticalExtensions;
	Set<String> nonCriticalExtensions;
	Set<String> altNameOids;
	Set<String> names;
	Set<String> otherNameOids;
	Set<String> certificatePolicies;
	
	private static Set<String> extendedKeyUsageSS = new LinkedHashSet<String>();
	private static Set<String> criticalExtensionsSS = new LinkedHashSet<String>();
	private static Set<String> nonCriticalExtensionsSS = new LinkedHashSet<String>();
	private static Set<String> altNameOidsSS = new LinkedHashSet<String>();
	private static Set<String> otherNameOidsSS = new LinkedHashSet<String>();
	private static Set<String> certificatePoliciesSS = new LinkedHashSet<String>();
	
	public DetalheCert(X509Certificate cert) throws IOException, CertificateParsingException {
		sigAlg = cert.getSigAlgName();
		sigOid = cert.getSigAlgOID();
		if(cert.getSigAlgParams()!=null){
			ASN1InputStream stream = new ASN1InputStream(cert.getSigAlgParams());
			DERObject derObj = stream.readObject();
		}
		keyUsage = cert.getKeyUsage();
		extendedKeyUsage = cert.getExtendedKeyUsage();
		extendedKeyUsageSS.addAll(extendedKeyUsage);
		
		criticalExtensions = cert.getCriticalExtensionOIDs();
		criticalExtensionsSS.addAll(criticalExtensions);
		
		nonCriticalExtensions = cert.getNonCriticalExtensionOIDs();
		nonCriticalExtensionsSS.addAll(criticalExtensions);
		
		
		byte[] bytesSubjectName = cert.getExtensionValue(X509Extensions.SubjectAlternativeName.getId());
		
		names = new LinkedHashSet<String>();
		otherNameOids = new LinkedHashSet<String>();
		
		// recupera subjectAltName oids
		ASN1InputStream oInput = new ASN1InputStream(new ByteArrayInputStream(bytesSubjectName));
		ASN1Sequence oSeq = null;
		ASN1OctetString oOct = ASN1OctetString.getInstance(oInput.readObject());
		oInput = new ASN1InputStream(new ByteArrayInputStream(oOct.getOctets()));
		oSeq = ASN1Sequence.getInstance(oInput.readObject());
		// Percorre a sequencia do "Subject Alternative Name"
		for (int i = 0; oSeq != null && i < oSeq.size(); i++) {
			ASN1TaggedObject oTagged = (ASN1TaggedObject) oSeq.getObjectAt(i);
			String name = nameTypes[oTagged.getTagNo()];
			if (oTagged.getObject() instanceof ASN1OctetString) {
				name+=" "+(new String(((ASN1OctetString) oTagged.getObject()).getOctets()));
			} else if(oTagged.getObject() instanceof DERSequence) {
				ASN1Sequence seq = ASN1Sequence.getInstance(oTagged.getObject());
				//ASN1ObjectIdentifier id = (ASN1ObjectIdentifier) seq.getObjectAt(0);
				//name+=" "+id.getId();
				//otherNameOids.add(id.getId());
			} else {
				System.out.println(oTagged.getObject().getClass());
			}
			names.add(name);
			otherNameOidsSS.addAll(otherNameOids);
		}
		//Gets the DER-encoded OCTET string for the extension value
		byte[] certificatePolicies = cert.getExtensionValue(X509Extensions.CertificatePolicies.getId());
		ASN1InputStream inDer = new ASN1InputStream(new ByteArrayInputStream(certificatePolicies));
		DEROctetString inOctetString = (DEROctetString) inDer.readObject();
		ASN1InputStream inDer2 = new ASN1InputStream(new ByteArrayInputStream(inOctetString.getOctets()));
		DERSequence derSeq = (DERSequence) inDer2.readObject();
		ASN1Sequence asn1Seq = ASN1Sequence.getInstance(derSeq);
		ASN1Sequence asn1Seq2 = ASN1Sequence.getInstance(asn1Seq.getObjectAt(0));
		//ASN1ObjectIdentifier oidPolicy = (ASN1ObjectIdentifier) asn1Seq2.getObjectAt(0);
		this.certificatePolicies = new LinkedHashSet<String>();
		//this.certificatePolicies.add(oidPolicy.getId());
		this.certificatePoliciesSS.addAll(this.certificatePolicies);
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		buf.append(sigAlg+",");
		buf.append(sigOid+",");
		for (int i = 0; i < keyUsage.length; i++) {
			if(keyUsage[i]){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		for (String i : extendedKeyUsageSS) {
			if(extendedKeyUsage.contains(i)){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		for (String i : criticalExtensionsSS) {
			if(criticalExtensions.contains(i)){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		for (String i : nonCriticalExtensionsSS) {
			if(nonCriticalExtensions.contains(i)){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		for (String i : otherNameOidsSS) {
			if(otherNameOids.contains(i)){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		for (String i : certificatePoliciesSS) {
			if(certificatePolicies.contains(i)){
				buf.append("1,");
			}else{
				buf.append("0,");
			}
		}
		
		return buf.toString();
	}
	
	public static String header() {
		StringBuffer header = new StringBuffer("sigAlg,sigOid,");
		for (int i = 0; i < keyUsageNames.length; i++) {
			header.append(keyUsageNames[i]+",");
		}
		
		for (String i : extendedKeyUsageSS) {
			header.append(getOidDesc(i)+",");
		}
		
		for (String i : criticalExtensionsSS) {
			header.append(getOidDesc(i)+",");
		}
		
		for (String i : nonCriticalExtensionsSS) {
			header.append(getOidDesc(i)+",");
		}
		for (String i : otherNameOidsSS) {
			header.append(getOidDesc(i)+",");
		}
		for (String i : certificatePoliciesSS) {
			header.append(getOidDesc(i)+",");
		}
		return header.toString();
	}
	
	public static String getOidDesc(String oid){
		for (int i = 0; i < oidToString.length; i++) {
			if(oidToString[i][0].equalsIgnoreCase(oid)){
				return oid+" - "+oidToString[i][1];
			}
		}
		return oid;
	}
}
