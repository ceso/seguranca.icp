package br.jus.stf.seguranca.icp.service.test;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.util.NtpInfo;
import br.jus.stf.seguranca.icp.util.TimeUtil;

@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
public class TestNtpService {
	
	@Test
	public void testNtpSucesso() throws IOException {
		TimeUtil.ntpOffset("ntpserver.stf.jus.br", 123, 10000);
	}
	
	@Test
	public void testNtpTimeout() throws IOException {
		try {
			TimeUtil.ntpOffset("a.ntp.br", 123, 10000);
			Assert.assertFalse(false);
		} catch(SocketTimeoutException e) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void testVariosServidores() {
		NtpInfo info = TimeUtil.ntpOffset(new String[]{"a.ntp.br","b.ntp.br","c.ntp.br","gps.ntp.br","ntpserver.stf.jus.br"}, 123, 10000);
		Assert.assertNotNull(info);
	}
}
