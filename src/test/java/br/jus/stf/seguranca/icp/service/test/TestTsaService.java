package br.jus.stf.seguranca.icp.service.test;



import java.security.NoSuchAlgorithmException;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.service.CarimbadoraService;
import br.jus.stf.seguranca.icp.service.exception.CarimbadoraException;
import br.jus.stf.seguranca.icp.util.CertUtil;
import br.jus.stf.seguranca.icp.util.TimeUtil;

@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@SuppressWarnings("restriction")
@Ignore
public class TestTsaService {
	
	private int port = 318;
	/**
	 * Carimbadora interna ao STF
	 */
	private String carimbadora1 = "201.49.148.134";
	
	/**
	 * Carimbadora existente no observatÃ³rio.
	 */
	@SuppressWarnings("unused")
	private String carimbadora2 = "200.160.7.195";
	
	/**
	 * Carimbadora de teste cadeia v2.
	 */
	private String carimbadora3 = "200.20.186.202";

	private CarimbadoraService carimbadoraService;
	
	@Test
	public void testCarimbadoras() throws NoSuchAlgorithmException {
		byte[] b = CertUtil.sha1Sum("teste".getBytes());
		Assert.assertNotNull(TimeUtil.carimbar(b, carimbadora1, port, true, null));
		//Carimbadora desativada em 21/05/2012 por nÃ£o suportar a cadeia v2.
		//Assert.assertNotNull(TimeUtil.carimbar(b, carimbadora2, port, true, null));
		Assert.assertNotNull(TimeUtil.carimbar(b, carimbadora3, port, true, null));
	}
	
	@Test
	public void testCarmibadoraService() throws CarimbadoraException {
		Assert.assertNotNull(this.carimbadoraService.carimbarDados("teste"));
	}

	public CarimbadoraService getCarimbadoraService() {
		return carimbadoraService;
	}

	@Resource(name="carimbadoraService")
	public void setCarimbadoraService(CarimbadoraService carimbadoraService) {
		this.carimbadoraService = carimbadoraService;
	}
	
}
