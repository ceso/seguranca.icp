package br.jus.stf.seguranca.icp.service.test;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.bouncycastle.cms.CMSException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.impl.IcpServiceImpl;
import br.jus.stf.seguranca.icp.util.CertUtil;

@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@SuppressWarnings("restriction")
@Ignore
public class TestP7b {
	
	private CadastroCertificadoService cadastroService;
	private IcpServiceImpl icpService;
	
	@Test
	public void testCarimbadoras() throws NoSuchAlgorithmException, IOException, InvalidAlgorithmParameterException, NoSuchProviderException, CertStoreException, CMSException {
		List<CertificadoDigital> ancoras = cadastroService.findAncorasConfiaveis();
		List<X509Certificate> certs = new LinkedList<X509Certificate>();
		for (CertificadoDigital certificadoDigital : ancoras) {
			certs.add(certificadoDigital.getX509());
		}
		byte[] p7b = CertUtil.genP7b(certs, null);
		Assert.assertNotNull(p7b);
	}

	public CadastroCertificadoService getCadastroService() {
		return cadastroService;
	}
	
	@Resource(name="cadastroService")
	public void setCadastroService(CadastroCertificadoService cadastroService) {
		this.cadastroService = cadastroService;
	}
	
	public IcpService getIcpService() {
		return icpService;
	}

	@Resource(name="icpService")
	public void setIcpService(IcpServiceImpl icpService) {
		this.icpService = icpService;
	}
}
