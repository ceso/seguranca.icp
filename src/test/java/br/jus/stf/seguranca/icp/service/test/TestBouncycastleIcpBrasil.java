package br.jus.stf.seguranca.icp.service.test;

import java.security.cert.X509Certificate;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.domain.AssinaturaDigital;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.impl.IcpServiceImpl;

@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@SuppressWarnings( { "restriction", "deprecation" })
@Ignore
public class TestBouncycastleIcpBrasil {
	
	private CadastroCertificadoService cadastroService;
	private IcpServiceImpl icpService;

	@Test
	public void testAll() throws Exception {
		List<AssinaturaDigital> assinaturasDigitais = cadastroService.findAllAssinaturaDigital();
		
		for (AssinaturaDigital ass : assinaturasDigitais) {
			X509Certificate x509 = ass.getX509();
			if(x509==null) {
				continue;
			}
			try {
				icpService.validarCertificado(new X509Certificate[] { x509 }, x509.getNotAfter(), false);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	public CadastroCertificadoService getCadastroService() {
		return cadastroService;
	}
	
	@Resource(name="cadastroService")
	public void setCadastroService(CadastroCertificadoService cadastroService) {
		this.cadastroService = cadastroService;
	}

	public IcpService getIcpService() {
		return icpService;
	}

	@Resource(name="icpServiceAcTeste")
	public void setIcpService(IcpServiceImpl icpService) {
		this.icpService = icpService;
	}
}
