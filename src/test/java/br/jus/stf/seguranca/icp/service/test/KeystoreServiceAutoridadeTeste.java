package br.jus.stf.seguranca.icp.service.test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.cert.X509Certificate;
import java.util.List;

import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.impl.KeystoreServiceAdapter;
import br.jus.stf.seguranca.icp.util.CertUtil;

public class KeystoreServiceAutoridadeTeste extends KeystoreServiceAdapter {

	private X509Certificate x509AcTeste;
	
	public KeystoreServiceAutoridadeTeste(KeyStoreService keyStoreService) {
		super(keyStoreService);
	}

	private X509Certificate getAcTesteCert() {
		if (x509AcTeste == null) {
			try {
				InputStream in = KeystoreServiceAutoridadeTeste.class.getResourceAsStream("/certificados/ancora/ac-teste.txt");
				InputStreamReader reader = new InputStreamReader(in);
				x509AcTeste = (X509Certificate) CertUtil.fromPem(reader);
			} catch (Exception e) {
				throw new RuntimeException("Falha ao recuperar ac-teste: " + e.getMessage(), e);
			}
		}
		return x509AcTeste;
	}

	@Override
	public List<X509Certificate> getTrustedAnchors() {
		List<X509Certificate> trustedAnchors = super.getTrustedAnchors();
		if(!trustedAnchors.contains(getAcTesteCert())){
			trustedAnchors.add(getAcTesteCert());
		}
		return trustedAnchors;
	}
	
	
}
