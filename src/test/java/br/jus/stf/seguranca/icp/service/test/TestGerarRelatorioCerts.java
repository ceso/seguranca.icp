package br.jus.stf.seguranca.icp.service.test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.jus.stf.seguranca.icp.domain.AssinaturaDigital;
import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.exception.SegurancaException;
import br.jus.stf.seguranca.icp.service.impl.IcpServiceImpl;

@ContextConfiguration(locations = { "/test-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@SuppressWarnings( { "restriction", "deprecation" })
@Ignore
public class TestGerarRelatorioCerts {
	
	private CadastroCertificadoService cadastroService;
	private IcpServiceImpl icpService;
	
	
	@Test
	public void testUsuarioCert() throws CertificateException, SegurancaException, IOException {
		List<DetalheCert> certs = new LinkedList<DetalheCert>();
		processarAssinaturas(certs);
		File f = new File("D:/Documents and Settings/leandro.oliveira/Desktop/relcerts.txt");
		PrintWriter writer = new PrintWriter(f);
		printDetalhes(certs, writer);
		writer.flush();
		writer.close();
	}
	
	private void processarAssinaturas(List<DetalheCert> detalhes) throws CertificateException, IOException {
		List<AssinaturaDigital> assinaturasDigitais = cadastroService.findAllAssinaturaDigital();
		for (AssinaturaDigital assinaturaDigital : assinaturasDigitais) {
			X509Certificate x509 = assinaturaDigital.getX509();
			if(x509!=null) {
				DetalheCert d = new DetalheCert(x509);
				detalhes.add(d);
			}
		}
	}
	
	private void printDetalhes(List<DetalheCert> detalhes, PrintWriter out) {
		out.println(DetalheCert.header());
		for (DetalheCert d : detalhes) {
			out.println(d);
		}
	}

	public CadastroCertificadoService getCadastroService() {
		return cadastroService;
	}
	
	@Resource(name="cadastroService")
	public void setCadastroService(CadastroCertificadoService cadastroService) {
		this.cadastroService = cadastroService;
	}

	public IcpService getIcpService() {
		return icpService;
	}

	@Resource(name="icpService")
	public void setIcpService(IcpServiceImpl icpService) {
		this.icpService = icpService;
	}

}
