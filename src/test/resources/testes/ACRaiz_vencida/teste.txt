# Estrutura do arquivo:
# Primeiro vem um especificador que pode ser: Certificados,Raizes,Arquivos-Validos,Certificados-Validos,Arquivos-Invalidos,Certificados-Invalidos,Crl
# Nas linhas seguintes, um caminho no classpath contendo itens do tipo especificado.
# Linhas em branco e comentarios sao ignorados.

Certificados
/testes/ACRaiz_vencida//final/final.cer
/testes/ACRaiz_vencida//intermediaria/intermediaria.cer
/testes/ACRaiz_vencida//raiz/raiz.cer

Raizes
/testes/ACRaiz_vencida//raiz/raiz.cer

Crl
/testes/ACRaiz_vencida//crls/ACRaiz_vencida.crl
/testes/ACRaiz_vencida//crls/ACRaiz_vencida_ACFinal.crl
/testes/ACRaiz_vencida//crls/ACRaiz_vencida_ACIntermediaria.crl

Arquivos-Invalidos
/testes/ACRaiz_vencida//arquivo/arquivo.pdf

Certificados-Invalidos
