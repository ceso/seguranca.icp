#!/bin/bash

if [ "$1" = "" -o "$2" = "" ]; then
	echo "Usage: $0 <diretorio> [valido|invalido]";
	exit 1;
fi

cd $1;

if [ ! -d arquivo -o ! -d crls -o ! -d raiz -o ! -d usuario ]; then
	echo "Falta criar a estrutura de pasta contendo os diretorios";
	echo "arquivo, com os pdfs";
	echo "crls, com as crls";
	echo "raiz, com as raizes";
	echo "usuario, com os certificados do usuario";
	exit 1;
fi

if [ -f teste.txt ]; then
	rm teste.txt;
fi
touch teste.txt;

echo "# Estrutura do arquivo:">>teste.txt;
echo "# Primeiro vem um especificador que pode ser: Certificados,Raizes,Arquivos-Validos,Certificados-Validos,Arquivos-Invalidos,Certificados-Invalidos,Crl">>teste.txt;
echo "# Nas linhas seguintes, um caminho no classpath contendo itens do tipo especificado.">>teste.txt;
echo "# Linhas em branco e comentarios sao ignorados.">>teste.txt;

echo "">>teste.txt;

echo "Certificados">>teste.txt;
find . -name '*.cer' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

echo "">>teste.txt;
echo "Raizes">>teste.txt;
find ./raiz -name '*.cer' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

echo "">>teste.txt;
echo "Crl">>teste.txt;
find ./crls -name '*.crl' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

if [ "$2" = "valido" ]; then
	echo "">>teste.txt;
	echo "Arquivos-Validos">>teste.txt;
	find ./arquivo -name '*.pdf' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

	echo "">>teste.txt;
	echo "Certificados-Validos">>teste.txt;
	find ./usuario -name '*.cer' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

fi

if [ "$2" = "invalido" ]; then
	echo "">>teste.txt;
	echo "Arquivos-Invalidos">>teste.txt;
	find ./arquivo -name '*.pdf' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

	echo "">>teste.txt;
	echo "Certificados-Invalidos">>teste.txt;
	find ./usuario -name '*.cer' -printf "%p\n" | sed "s|\./|/testes/$1/|" >>teste.txt;

fi

