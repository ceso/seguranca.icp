# Estrutura do arquivo:
# Primeiro vem um especificador que pode ser: Certificados,Raizes,Arquivos-Validos,Certificados-Validos,Arquivos-Invalidos,Certificados-Invalidos,Crl
# Nas linhas seguintes, um caminho no classpath contendo itens do tipo especificado.
# Linhas em branco e comentarios sao ignorados.

Certificados
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//final/final.cer
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//intermediaria/intermediaria.cer
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//raiz/raiz.cer
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//usuario/usuario.cer

Raizes
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//raiz/raiz.cer

Crl
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//crls/final.crl
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//crls/gasCA_Raiz.crl
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//crls/intermediaria.crl

Arquivos-Invalidos
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//arquivo/arquivo.pdf

Certificados-Invalidos
/testes/CAGASv2_CAGASIntermediaria_CAGASFinal_CertExpirado//usuario/usuario.cer
