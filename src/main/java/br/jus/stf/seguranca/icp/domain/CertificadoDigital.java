package br.jus.stf.seguranca.icp.domain;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Representa os diversos certificados digitais (X.509) e seus respectivos
 * tipos.
 * 
 * @author Leandro.Oliveira
 */
@Entity
@Table(schema = "CORP", name = "CERTIFICADO_DIGITAL")
public class CertificadoDigital implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String serial;
	private String descricao;
	private TipoCertificadoDigital tipo;
	private Date inicioValidade;
	private Date fimValidade;
	private char[] certificado;
	private CertificadoDigital emissor;
	private List<CentroDistribuicaoCRL> centrosDistribuicao = new LinkedList<CentroDistribuicaoCRL>();
	private Long seqUsuarioExterno;

	@Id
	@Column(name = "SEQ_CERTIFICADO_DIGITAL")
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", sequenceName = "CORP.SEQ_CERTIFICADO_DIGITAL", allocationSize = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "COD_SERIAL", length = 60)
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	@Column(name = "DSC_CERTIFICADO_DIGITAL", length = 255)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TIP_CERTIFICADO_DIGITAL")
	public TipoCertificadoDigital getTipo() {
		return tipo;
	}

	public void setTipo(TipoCertificadoDigital tipo) {
		this.tipo = tipo;
	}

	@Column(name = "DAT_VALIDADE_INICIAL")
	public Date getInicioValidade() {
		return inicioValidade;
	}

	public void setInicioValidade(Date inicioValidade) {
		this.inicioValidade = inicioValidade;
	}

	@Column(name = "DAT_VALIDADE_FINAL")
	public Date getFimValidade() {
		return fimValidade;
	}

	public void setFimValidade(Date fimValidade) {
		this.fimValidade = fimValidade;
	}

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "TXT_CERTIFICADO_DIGITAL")
	public char[] getCertificado() {
		return certificado;
	}

	public void setCertificado(char[] certificado) {
		this.certificado = certificado;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SEQ_CERTIFICADO_EMISSOR", nullable = true)
	public CertificadoDigital getEmissor() {
		return emissor;
	}

	public void setEmissor(CertificadoDigital emissor) {
		this.emissor = emissor;
	}

	@OneToMany(mappedBy = "certificado",cascade=CascadeType.ALL)
	public List<CentroDistribuicaoCRL> getCentrosDistribuicao() {
		return centrosDistribuicao;
	}

	public void setCentrosDistribuicao(List<CentroDistribuicaoCRL> centrosDistribuicao) {
		this.centrosDistribuicao = centrosDistribuicao;
	}
	
	@Column(name = "SEQ_USUARIO_EXTERNO", length = 60)
	public Long getSeqUsuarioExterno() {
		return seqUsuarioExterno;
	}

	public void setSeqUsuarioExterno(Long seqUsuarioExterno) {
		this.seqUsuarioExterno = seqUsuarioExterno;
	}

	@Transient
	public void addCentroDistribuicao(URL url) {
		// impede a entrada de urls nulas
		if (url == null) {
			return;
		}
		// impede a entrada de urls repetidas
		for (CentroDistribuicaoCRL cdp : getCentrosDistribuicao()) {
			if (cdp.getUrl().equals(url.toExternalForm())) {
				return;
			}
		}
		CentroDistribuicaoCRL centro = new CentroDistribuicaoCRL();
		centro.setAtivo(Boolean.TRUE);
		centro.setCertificado(this);
		centro.setUrl(url.toExternalForm());
		getCentrosDistribuicao().add(centro);
	}

	@Transient
	public void removeCentroDistribuicao(URL url) {
		if (url == null) {
			return;
		}
		Iterator<CentroDistribuicaoCRL> it = getCentrosDistribuicao().iterator();
		while (it.hasNext()) {
			CentroDistribuicaoCRL cdp = it.next();
			if (cdp.getUrl().equals(url.toExternalForm())) {
				it.remove();
			}
		}
	}
	
	@Transient
	public X509Certificate getX509() throws IOException {
		if (getCertificado() == null || getCertificado().length == 0) {
			return null;
		}
		return (X509Certificate) CertUtil.fromPem(getCertificado());
	}
}
