package br.jus.stf.seguranca.icp.service.impl.crlmanager;

import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;

import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Handler para resposta do download da CRL não armazenada em zip, mas no formato padrão de download.
 * 
 * @author Leandro.Oliveira
 */
public class X509CrlResponseHandler implements ResponseHandler<X509CRL> {

	private final Log logger = LogFactory.getLog(X509CrlResponseHandler.class);

	public X509CRL handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		StatusLine status = response.getStatusLine();
		// se status diferente de 200
		if (status.getStatusCode() != HttpStatus.SC_OK) {
			// gera erro
			throw new HttpResponseException(status.getStatusCode(), response.getStatusLine().getReasonPhrase());
		} else {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream entityContent = null;
				try {
					CertUtil.initBouncycastleProvider();
					entityContent = entity.getContent();
					CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509",Security.getProvider(CertUtil.PROVIDER_BC_NAME));
					return (X509CRL) certificateFactory.generateCRL(entityContent);
				} catch (CertificateException e) {
					logger.error("Erro ao popular lista de CRLs.", e);
					throw new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				} catch (CRLException e) {
					logger.error("Erro ao popular lista de CRLs.", e);
					throw new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				} finally {
					if (entityContent != null) {
						entityContent.close();
					}
				}
			}
		}
		return null;
	}

}
