package br.jus.stf.seguranca.icp.service.exception;

/**
 * Marca CRLs invalidas, venciadas ou cujo emissor nao confere com o esperado.
 * 
 * @author Leandro.Oliveira
 */
public class CRLInvalidaException extends SegurancaException {
	
	private static final long serialVersionUID = 1L;

	public CRLInvalidaException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}

	public CRLInvalidaException(String mensagem) {
		super(mensagem);
	}
}
