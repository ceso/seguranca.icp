package br.jus.stf.seguranca.icp.service.impl.crlmanager;

import java.net.URL;
import java.security.cert.X509CRL;

/**
 * Representa a crl, a url por que foi baixada.
 * Ou a URL que por Ãºltimo impossibilitou a baixa junto com a exception.
 * Sempre Ã© retornada quantidade de URLs utilizadas na pesquisa.
 * 
 * @author Leandro.Oliveira
 */
public class CrlDto {

	private X509CRL crl;
	private URL url;
	private Exception exception;
	private int qtdUrlsProcuradas;

	/**
	 * Construtor.
	 * @param crl X509CRL
	 * @param url URL
	 * @param exception Exception
	 */
	public CrlDto(X509CRL crl, URL url, Exception exception, int qtdUrlsProcuradas) {
		this.crl = crl;
		this.url = url;
		this.exception = exception;
		this.qtdUrlsProcuradas = qtdUrlsProcuradas;
	}

	public X509CRL getCrl() {
		return crl;
	}

	public URL getUrl() {
		return url;
	}

	public Exception getException() {
		return exception;
	}

	public int getQtdUrlsProcuradas() {
		return qtdUrlsProcuradas;
	}
}
