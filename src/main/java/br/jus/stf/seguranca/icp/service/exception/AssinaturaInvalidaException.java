package br.jus.stf.seguranca.icp.service.exception;


/**
 * Identifica erros de assinatura.
 * 
 * @author Leandro.Oliveira
 * 
 */
public class AssinaturaInvalidaException extends SegurancaException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Construtor.
	 * @param mensagem
	 * @param mensagemDetalhada
	 */
	public AssinaturaInvalidaException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}
	
	/**
	 * Construtor.
	 * @param mensagem
	 */
	public AssinaturaInvalidaException(String mensagem){
		super(mensagem);
	}
	
}
