package br.jus.stf.seguranca.icp.domain;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Representa as entidades intermediÃ¡rias bem como entidades iniciais para
 * emissÃ£o de certificados. Classe depreciada, a classe CertificadoDigital
 * deverÃ¡ substituir esta.
 * 
 * @deprecated
 * @author Leandro.Oliveira
 * 
 */
@Entity
@Table(name = "CORP.AUTORIDADE_CERTIFICADORA")
public class AutoridadeCertificadora implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String alias;
	private byte[] cadeiaCertificacao;
	private String numeroSerie;
	private byte[] listaRevogacao;

	@Id
	@SequenceGenerator(name = "AUTORIDADE_CERTIFICADORA_SEQAUTORIDADECERTIFICADORA_GENERATOR", sequenceName = "CORP.SEQ_AUTORIDADE_CERTIFICADORA", allocationSize = 0)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTORIDADE_CERTIFICADORA_SEQAUTORIDADECERTIFICADORA_GENERATOR")
	@Column(name = "SEQ_AUTORIDADE_CERTIFICADORA", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DSC_AUTORIDADE_CERTIFICADORA", nullable = false, length = 255)
	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Lob()
	@Column(name = "BIN_CADEIA_CERTIFICACAO")
	public byte[] getCadeiaCertificacao() {
		return cadeiaCertificacao;
	}

	public void setCadeiaCertificacao(byte[] cadeiaCertificacao) {
		this.cadeiaCertificacao = cadeiaCertificacao;
	}

	@Lob()
	@Column(name = "BIN_LISTA_REVOGACAO")
	public byte[] getListaRevogacao() {
		return listaRevogacao;
	}

	public void setListaRevogacao(byte[] listaRevogacao) {
		this.listaRevogacao = listaRevogacao;
	}

	@Column(name = "COD_SERIAL")
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Transient
	public X509Certificate getX509() throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(getCadeiaCertificacao()));
	}
}
