package br.jus.stf.seguranca.icp.service.impl.crlmanager;

import java.io.IOException;


import java.net.URL;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.http.client.HttpClient;

import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.util.CertUtil;


/**
 * Gerencia o download de CRLs para um certificado.
 * Recebe um Certificado na criação da classe.
 * Identifica as URLs dos Centros de distribuição.
 * Faz download usando um Executor de threads.
 * 
 * 
 * @author Leandro.Oliveira
 */
public class CrlDownloadManager {
	/** Certificado de onde foram obtidos os CDPs.*/
	private X509Certificate certificate;
	/** Indica se as URLs foram obtidas a partir do <code>certificate</code> */
	private boolean cdpsFromCertificate = false;
	/** Última CRL encontrada. */
	private X509CRL crl;
	/** URL de onde foi baixada a ultima CRL*/
	private URL url;
	/** Data em que a CRL foi baixada. */
	private Date crlDownloadDate;
	/** Exception que impediu a baixa da CRL por determinada URL.*/
	private Exception exception;
	/** Ponteiro para os threads que realizam o download. */
	private List<GetterThread> getterThreads = new LinkedList<GetterThread>();
	/** Executor para ser utilizado no download.*/
	private ScheduledExecutorService executor;
	/** Cliente http utilizado para download. */
	private HttpClient httpClient;
	/** Tempo de espera pelo download. Método waitForCrl.*/
	private long waitTime;
	
	/**
	 * Construtor de um CrlDownloadManager
	 * @param cert X509Certificate - certificado que contém os CDPs.
	 * @param httpClient HttpClient.
	 * @param executor ScheduledExecutor
	 * @param period long - Período do download em minutos.
	 * @param cdpsFromCertificate boolean - Indica que as URLs foram obtidas a partir do <code>cert</code>
	 * @throws IOException
	 */
	public CrlDownloadManager(X509Certificate cert, HttpClient httpClient, ScheduledExecutorService executor, boolean cdpsFromCertificate, long waitTime) throws IOException {
		this.certificate = cert;
		this.executor = executor;
		this.httpClient = httpClient;
		this.cdpsFromCertificate = cdpsFromCertificate;
		this.waitTime = waitTime;
	}
	
	/**
	 * Adiciona uma nova URL para download. Evita que URLs repetidas sejam adicionadas.
	 * @param url URL
	 */
	public synchronized void addUrl(URL url) {
		if (url == null) {
			return;
		}
		// percorre todas os threads
		for (GetterThread getterThread : getterThreads) {
			// se ja existir um thread para a url, nao cria outros
			if(getterThread.getUrl().equals(url) || getterThread.getUrl().toExternalForm().equals(url.toExternalForm())) {
				return;
			}
		}
		GetterThread getterThread = new GetterThread(this, url, httpClient, executor);
		getterThreads.add(getterThread);
		executor.execute(getterThread);
	}

	/**
	 * Retorna a CRL atual.
	 * @return CrlDto
	 */
	public synchronized CrlDto getCrlDto() {
		return new CrlDto(crl, url, exception, getterThreads.size());
	}

	/**
	 * Retorna o certificado.
	 * @return
	 */
	public X509Certificate getCertificate() {
		return certificate;
	}

	/**
	 * Indica se os CDPs foram retirados do <code>certificate</code>
	 * @return boolean
	 */
	public boolean isCdpsFromCertificate() {
		return cdpsFromCertificate;
	}

	/**
	 * Verifica se a CRL atual é válida.
	 * @return boolean
	 */
	public synchronized boolean isCrlValida() {
		if (crl == null) {
			return false;
		}
		try {
			CertUtil.validarDataCRL(crl);
		} catch (CRLInvalidaException e) {
			return false;
		}
		return true;
	}

	/**
	 * Altera a CRL. Impede apenas CRLs inválidas. Altera a data de atualização da CRL.
	 * @param crl
	 * @throws CRLInvalidaException
	 */
	public synchronized void setCrl(X509CRL crl, URL url) throws CRLInvalidaException {
		// Impede que seja atualizado com null
		if (crl == null) {
			return;
		}
		// necessario para garantir que a crl que substituirá a antiga possui data mais atual 
		if(this.crl != null && crl!=null) {
			if(this.crl.getNextUpdate().after(crl.getNextUpdate())) {
				return;
			}
		}
		
		// Se os CDPs foram obtidos do certificado
		if (cdpsFromCertificate) {
			// Verifica se possuem o mesmo emissor o certificado e a CRL, se não, rejeita.
			if (!certificate.getIssuerX500Principal().getName().equals(crl.getIssuerX500Principal().getName())) {
				throw new CRLInvalidaException(String.format("A CRL [%s] recuperada não possui relação com o certificado [%s] emitido por [%s].", CertUtil.getCrlString(crl), CertUtil.getCertString(certificate), certificate
						.getIssuerX500Principal().getName()));
			}
		} else {
			// Se a CRL não foi emtida pelo certificado
			if (!CertUtil.isIssuer(crl, certificate)) {
				throw new CRLInvalidaException(String.format("A CRL [%s] recuperada não possui relação com o certificado [%s] emitido por [%s].", CertUtil.getCrlString(crl), CertUtil.getCertString(certificate), certificate
						.getIssuerX500Principal().getName()));
			}
		}
		this.crl = crl;
		this.url = url;
		this.crlDownloadDate = new Date();
		// Zera a exception
		this.exception = null;
		synchronized (this.certificate) {
			this.certificate.notifyAll();
		}
	}
	
	/**
	 * Notifica que houve um erro na baixa de uma CRL.
	 * @param url URL
	 * @param e Exception
	 */
	public synchronized void setException(URL url, Exception e) {
		if (isCrlValida()) {
			return;
		}
		this.url = url;
		this.exception = e;
		synchronized (this.certificate) {
			this.certificate.notifyAll();
		}
	}

	/**
	 * Retorna a data em que a crl foi atualizada neste objeto.
	 * @return Date
	 */
	public synchronized Date getCrlDownloadDate() {
		return crlDownloadDate;
	}

	/**
	 * Faz com que o thread corrente espere o próximo download do certificado.
	 * @param cert
	 * @throws InterruptedException
	 */
	public void waitForCrl() throws InterruptedException {
		if (!isCrlValida()) {
			synchronized (this.certificate) {
				this.certificate.wait(waitTime);	
			}
		}
	}
}
