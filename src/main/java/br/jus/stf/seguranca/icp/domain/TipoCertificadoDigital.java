package br.jus.stf.seguranca.icp.domain;

/**
 * Tipos de certificados digitais.
 * Raiz: são as ancoras confiáveis, certificados auto-assinados.
 * Autoridade Certificadora: certificados de autoridades intermediárias.
 * Entidade Final: certificado de usuários ou máquinas.
 * 
 * @author Leandro.Oliveira
 */
public enum TipoCertificadoDigital {
	R("Raiz"), 
	
	A("Autoridade Certificadora"), 
	
	F("Entidade Final");
	
	private String descricao;

	private TipoCertificadoDigital(String desc) {
		this.descricao = desc;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
