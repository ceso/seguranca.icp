package br.jus.stf.seguranca.icp.service.exception;

public class TimeStampException extends Exception {
	private String mensagem;
	
	public TimeStampException() {
		// TODO Auto-generated constructor stub
	}
	public TimeStampException(String mensagem) {
		// TODO Auto-generated constructor stub
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
