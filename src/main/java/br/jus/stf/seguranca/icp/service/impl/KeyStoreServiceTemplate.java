package br.jus.stf.seguranca.icp.service.impl;

import java.math.BigInteger;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Template para criação de implementações de KeyStoreService.
 * O método getTrustedAnchors gera UnsupportedOperation se não for possível montar a lista ancoras.
 * Necessário, já que muito pouco pode ser feito sem que as raízes confiáveis sejam recuperadas.
 * 
 * Existe uma trava contra um possível loop infinito no método getCertificateChain, limitando em 70 itens para
 * uma cadeia, o que na prática é muito pouco provável de acontecer.
 * 
 * Armazena as autoridades raízes em uma Lista e sincroniza a inicialização desta para otimizar a performance.
 * 
 * Para montar a lista de CRLs, percorre uma cadeia, e para cada item da cadeia, recupera a respectiva CRL, não
 * gera nenhum erro se para determinado item da cadeia não for possível recuperar uma CRL, porém, verifica a validade
 * da CRL.
 * 
 * @author Leandro.Oliveira
 */
public abstract class KeyStoreServiceTemplate implements KeyStoreService{
	private final Log logger = LogFactory.getLog(KeyStoreServiceTemplate.class);
	/**
	 * Mapa que contém o dn da autoridade certificadora raiz e como chave o serial.
	 */
	private Map<String, String> autoridadesRaizes;
	/**
	 * Cache com as autoridades raizes indicadas por <code>autoridadesRaizes</code>.
	 * Evita ir ao banco sempre.
	 */
	private List<X509Certificate> autoridadesCerts;
	
	/**
	 * @throws CertificadoInvalidoException 
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateChain(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.security.cert.X509Certificate)
	 */
	public List<X509Certificate> getCertificateChain(X509Certificate cert) throws CertificadoInvalidoException, CadeiaException {
		List<X509Certificate> chain = new LinkedList<X509Certificate>();
		X509Certificate currentCert = cert;
		//controla apenas para nao entrar em loop infinito (ocupando 100% de cpu)
		int controle = 0;
		while (true) {
			// se o certificado corrente for null (não existir na base, por exemplo)
			if (currentCert == null) {
				break;
			}
			logger.debug(String.format("Chain Array: [%s]", CertUtil.getCertString(currentCert)));
			chain.add(currentCert);
			// se for folha, pára o loop
			if (CertUtil.isRaiz(currentCert)) {
				break;
			}
			X500Principal issuerPrincipal = currentCert.getIssuerX500Principal();
			// se não houver issuer, (provavelmente um erro no certificado), pára o loop
			if (issuerPrincipal == null || issuerPrincipal.getName() == null || issuerPrincipal.getName().trim().length() == 0) {
				break;
			}
			X509Certificate issuer = getCertificateBySubjectName(issuerPrincipal.getName());
			if (issuer != null && !CertUtil.isIssuer(currentCert, issuer)) {
				throw new CadeiaException(String.format("Falha ao verificar issuer. Certificado: [%s]. Emissor: [%s].",CertUtil.getCertString(currentCert),CertUtil.getCertString(issuer)));
			}
			if (issuer == null) {
				throw new CadeiaException("Não foi possível recuperar na base de cadastro do STF a cadeia completa. Certificado procurado: " + currentCert.getIssuerX500Principal().getName());
			}
			// atualiza o certificado corrente
			currentCert = issuer;
			if(controle++==70) {
				throw new CadeiaException("Trava contra loop infinito, o código que recupera a cadeia completou 70 chamadas. ");
			}
		}
		return chain;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate)
	 */
	public X509CRL getCrl(X509Certificate cert) throws CRLInvalidaException {
		return getCrl(cert, new Date());
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrlList(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.util.List)
	 */
	public List<X509CRL> getCrlList(List<X509Certificate> chain) throws CRLInvalidaException {
		return getCrlList(chain, new Date());
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrlList(java.util.List, java.util.Date)
	 */
	public List<X509CRL> getCrlList(List<X509Certificate> chain, Date data) throws CRLInvalidaException {
		List<X509CRL> crlList = new LinkedList<X509CRL>();
		for (X509Certificate cert : chain) {
			X509CRL crl = getCrl(cert, data);
			if (crl != null) {
				CertUtil.validarDataCRL(crl, data);
				// Verifica o emissor da crl
				if (!CertUtil.isIssuer(crl, cert)) {
					throw new CRLInvalidaException(String.format("A crl recuperada para [%s] não foi emitida por ele.", CertUtil.getCertString(cert)));
				}
				logger.debug(String.format("CRL recuperada para [%s].",CertUtil.getCertString(cert)));
				crlList.add(crl);
			} else {
				logger.debug(String.format("CRL não recuperada para [%s].", CertUtil.getCertString(cert)));
			}
		}
		return crlList;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getTrustedAnchors(br.jus.stf.seguranca.icp.service.KeyStoreContext)
	 */
	public List<X509Certificate> getTrustedAnchors() {
		if (autoridadesCerts != null && autoridadesCerts.size() > 0) {
			return autoridadesCerts;
		}
		synchronized (this) {
			if (autoridadesCerts != null && autoridadesCerts.size() > 0) {
				return autoridadesCerts;
			}
			List<X509Certificate> confiaveis = new ArrayList<X509Certificate>();
			Set<Entry<String, String>> entrySet = autoridadesRaizes.entrySet();
			for (Entry<String, String> entry : entrySet) {
				X509Certificate cert;
				try {
					cert = getCertificateBySubjectName(entry.getKey());
				} catch (CertificadoInvalidoException e) {
					throw new IcpRuntimeException("Falha ao recuperar autoridades certificadoras.",e);
				}
				if (cert == null) {
					//necessário para garantir que as implementações indiquem que não são capazes de montar o trustedAnchors
					throw new UnsupportedOperationException(String.format("Não foi possível recuperar certificado confiável [%s].", entry.getKey()));
				}
				// testes de seguranca para verificar se houve algum erro no cadastro da AC Raiz no banco de dados
				if (!new BigInteger(entry.getValue()).equals(cert.getSerialNumber())) {
					throw new IllegalArgumentException(String.format("O serial [%s] da AC Raiz [%s] não confere com o que foi obtido pelo DAO: [%s].", cert.getSerialNumber()
							.toString(), entry.getKey(), entry.getValue()));
				}
				if (!entry.getKey().equals(cert.getSubjectX500Principal().getName())) {
					throw new IllegalArgumentException(String.format("O dn passado na configuração [%s] não confere com o que foi retornado pelo banco [%s].", entry.getKey(), cert
							.getSubjectX500Principal().getName()));
				}
				// se passou nos testes, adiciona à coleção
				confiaveis.add(cert);
			}
			autoridadesCerts = confiaveis;
		}
		return autoridadesCerts;
	}

	public Map<String, String> getAutoridadesRaizes() {
		return autoridadesRaizes;
	}

	public void setAutoridadesRaizes(Map<String, String> autoridadesRaizes) {
		this.autoridadesRaizes = autoridadesRaizes;
	}
}
