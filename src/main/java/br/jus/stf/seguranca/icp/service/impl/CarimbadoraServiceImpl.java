package br.jus.stf.seguranca.icp.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bouncycastle.tsp.TimeStampToken;

import br.jus.stf.seguranca.icp.service.CarimbadoraService;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.exception.CarimbadoraException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;
import br.jus.stf.seguranca.icp.util.CertUtil;
import br.jus.stf.seguranca.icp.util.TimeUtil;

/**
 * 
 * @author Leandro.Oliveira
 */
public class CarimbadoraServiceImpl implements CarimbadoraService {
	
	private String carimbadoras;
	private IcpService icpService;
	private Map<String,Integer> carimbadorasCache = new LinkedHashMap<String, Integer>();
	
	public TimeStampToken carimbarDados(String data) throws CarimbadoraException {
		try {
			byte[] d = CertUtil.sha1Sum(data.getBytes());
			return carimbarDigesto(d);
		} catch (NoSuchAlgorithmException e) {
			throw new IcpRuntimeException("Falha ao gerar sha1.", e);
		}
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CarimbadoraService#carimbarDigesto(byte[])
	 */
	public TimeStampToken carimbarDigesto(byte[] digesto) throws CarimbadoraException {
		Set<Entry<String, Integer>> tsas = carimbadorasCache.entrySet();
		for (Entry<String, Integer> entry : tsas) {
			TimeStampToken token = TimeUtil.carimbar(digesto, entry.getKey(), entry.getValue(), true, null);
			if (token != null) {
				return token;
			}
		}
		throw new CarimbadoraException("Nenhuma carimbadora respondeu a requisição.");
	}

	public String getCarimbadoras() {
		return carimbadoras;
	}

	public void setCarimbadoras(String carimbadoras) {
		this.carimbadoras = carimbadoras;
		String[] urls = carimbadoras.trim().split(",");
		for (int i = 0; i < urls.length; i++) {
			String[] url = urls[i].trim().split(":");
			carimbadorasCache.put(url[0], Integer.parseInt(url[1]));
		}
	}

	public IcpService getIcpService() {
		return icpService;
	}

	public void setIcpService(IcpService icpService) {
		this.icpService = icpService;
	}
}
