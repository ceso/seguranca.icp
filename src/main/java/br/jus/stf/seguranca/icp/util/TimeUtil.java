package br.jus.stf.seguranca.icp.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.tsp.TSPAlgorithms;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;

import br.jus.stf.seguranca.icp.util.ts.NtpMessage;

/**
 *  A troca de mensagens entre cliente e servidor permite que o cliente descubra qual seu deslocamento (offset) em relação ao servidor, 
 *  ou seja, o quanto seu relógio local difere do relógio do servidor.
 *  
 *  
 * @author Leandro.Oliveira
 */
public class TimeUtil {
	
	private static final Log logger = LogFactory.getLog(TimeUtil.class);
	
	/** Timeout de conexão padrão de 7s*/
	private static int connectionTimeout = 60000;
	
	/**
	 * Retorna o primeiro deslocamento retornado por uma lista de servidores.
	 * Utiliza a mesma porta e o mesmo timeout para todos.
	 * Se nenhum offset for retornado, retorna null.
	 * 
	 * @param servers String[]
	 * @param port int
	 * @param timeout int
	 * @return Double
	 */
	public static NtpInfo ntpOffset(String[] servers, int port, int timeout) {
		for (int i = 0; i < servers.length; i++) {
			try {
				NtpInfo info = ntpOffset(servers[i], port, timeout);
				if(info!=null) {
					return info;
				}
			} catch(IOException e) {
				logger.debug("IOException ao acessar servidor de tempo: "+servers[i], e);
				continue;
			}
		}
		return null;
	}
	
	/**
	 * Retorna o deslocamento do tempo local em relação ao servidor.
	 * 
	 * @param serverName String
	 * @param port int
	 * @param timeout int
	 * @return Double
	 * @throws IOException
	 */
	public static NtpInfo ntpOffset(String serverName, int port, int timeout) throws IOException {
		DatagramSocket socket = null;
		try {
			// Send request
			socket = new DatagramSocket();
			socket.setSoTimeout(timeout);
			InetAddress address = InetAddress.getByName(serverName);
			byte[] buf = new NtpMessage().toByteArray();
			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 123);

			// Set the transmit timestamp *just* before sending the packet
			// ToDo: Does this actually improve performance or not?
			NtpMessage.encodeTimestamp(packet.getData(), 40, (System.currentTimeMillis() / 1000.0) + 2208988800.0);

			socket.send(packet);

			// Get response
			packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);

			// Immediately record the incoming timestamp
			double destinationTimestamp = (System.currentTimeMillis() / 1000.0) + 2208988800.0;

			// Process response
			NtpMessage msg = new NtpMessage(packet.getData());

			double localClockOffset = ((msg.receiveTimestamp - msg.originateTimestamp) + (msg.transmitTimestamp - destinationTimestamp)) / 2;
			socket.close();
			NtpInfo info = new NtpInfo();
			info.setOffsetMillis(localClockOffset*1000);
			info.setReferenceCalendar(NtpMessage.timestampToCalendar(msg.referenceTimestamp));
			return info;
		} finally {
			if (socket != null) {
				socket.close();
			}
		}
	}
	
	public static TimeStampToken carimbar(byte[] digesto, String host, int port, boolean certReq, String reqPolicy) {
		TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();
		reqGen.setCertReq(true);
		if (reqPolicy != null) {
			reqGen.setReqPolicy(reqPolicy);
		}		
		TimeStampRequest request = reqGen.generate(TSPAlgorithms.SHA1, digesto);
		
		TimeStampResponse response = tcpTsaRequest(request, host, port);
		if(response == null) {
			logger.error(String.format("Erro ao acessar Time Stamp Authority %s:%d. Resposta NULL.", host, port));
			return null;
		}
		if (response.getStatus()!=0) {
			logger.error(String.format("Erro ao acessar Time Stamp Authority %s:%d. Status %d. Status str %s.", host, port, response.getStatus(), response.getStatusString()));
			return null;
		}
		TimeStampToken respToken = response.getTimeStampToken();
		return respToken;
	}

	/**
	 * Faz a requisição à carimbadora por TCP.
	 * @param request TimeStampRequest
	 * @param host String
	 * @param port int
	 * @return TimeStampResponse
	 */
	private static TimeStampResponse tcpTsaRequest(TimeStampRequest request, String host, int port) {
		DataInputStream datainputstream = null;
		DataOutputStream dataoutputstream = null;
		Socket socket = null;
		try {
			// Recupera os dados da requisicao
			byte[] dataReq = request.getEncoded();
			socket = new Socket();
			logger.debug(String.format("Acessando Time Stamp Authority %s:%d.", host, port));
			// Abre o socket
			socket.connect(new InetSocketAddress(host, port), connectionTimeout);
			datainputstream = new DataInputStream(socket.getInputStream());
			dataoutputstream = new DataOutputStream(socket.getOutputStream());

			// Envia os dados
			dataoutputstream.writeInt(dataReq.length + 1); // length (32-bits)
			dataoutputstream.writeByte(0); // flag (8-bits) '00'H TSA message
			dataoutputstream.write(dataReq); // value (data)
			dataoutputstream.flush();

			int respLen = datainputstream.readInt();
			byte flg = datainputstream.readByte();
			
			if(flg<0||flg>6) {
				logger.warn("TCP-based TSA message FLAG não previsto: "+flg);
			}
			
			// recupera os dados da resposta
			byte dataRsp[] = new byte[respLen - 1];
			datainputstream.readFully(dataRsp);
			// Recupera e valida a resposta
			TimeStampResponse resp = new TimeStampResponse(dataRsp);
			resp.validate(request);
			return resp;
		} catch (IOException e) {
			logger.error(String.format("Erro ao acessar Time Stamp Authority %s:%d.", host, port), e);
		} catch (TSPException e) {
			logger.error(String.format("TSPException ao acessar Time Stamp Authority %s:%d.", host, port), e);
		} finally {
			if (datainputstream != null) {
				try {
					datainputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (dataoutputstream != null) {
				try {
					dataoutputstream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
