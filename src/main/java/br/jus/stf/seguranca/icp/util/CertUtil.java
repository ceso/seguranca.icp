package br.jus.stf.seguranca.icp.util;

import java.io.ByteArrayInputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERString;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMWriter;

import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoExpiradoException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;

import com.itextpdf.text.pdf.PdfPKCS7;

/**
 * Classe utilitÃ¡ria para certificados. ContÃ©m mÃ©todos estÃ¡ticos, cujo trabalho
 * nÃ£o possui nenhuma implementaÃ§Ã£o diferente, e que nÃ£o ficam bem na classe
 * service.
 * 
 * @author Leandro.Oliveira
 * 
 */
public class CertUtil {

	// OID (Object Identifier) do CPF (de acordo com especificaÃ§Ã£o da
	// ICP-Brasil)
	private static final String OID_CPF = "2.16.76.1.3.1";

	// OID (Object Identifier) do CNPJ (de acordo com especificaÃ§Ã£o da
	// ICP-Brasil)
	private static final String OID_CNPJ = "2.16.76.1.3.3";

	// OID (Object Identifier) da OAB (de acordo com especificaÃ§Ã£o da
	// ICP-Brasil)
	private static final String OID_OAB = "2.16.76.1.4.";

	// Campo do OtherName do Subject Alternative Name
	private static final int TYPE_OTHERNAME = 0;

	// Campo do OtherName do Subject Alternative Name
	private static final int TYPE_RFC822_NAME = 1;
	
	// Logger
	private static final Log logger = LogFactory.getLog(CertUtil.class);
	
	// Bouncycastle
	public static final String PROVIDER_BC_NAME = "BC";
	public static final String PROVIDER_BC_CLASS = "org.bouncycastle.jce.provider.BouncyCastleProvider";

	
	/**
	 * Inicializa o bouncycastle provider se ele ainda nÃ£o estiver configurado.
	 * Em seguida, podem ser chamados mÃ©todos especÃ­ficos de seguranÃ§a indicando o bouncycastle que serÃ¡ utilizado.
	 * Ex:
	 * <ol> 
	 * 		<li>CertificateFactory.getInstance("X.509", CertUtil.PROVIDER_BC_NAME)</li>
	 *  	<li>CertPathValidator.getInstance("PKIX", CertUtil.PROVIDER_BC_NAME)</li>
	 * </ol>
	 */
	public static void initBouncycastleProvider() {
		if (Security.getProvider(PROVIDER_BC_NAME) == null) {
			try {
				Provider provider = (Provider) Class.forName(PROVIDER_BC_CLASS).newInstance();
				Security.addProvider(provider);
			} catch (Exception e) {
				logger.error(e);
				throw new IcpRuntimeException("NÃ£o foi possÃ­vel carregar o provider BouncyCastle.");
			}
		}
	}

	/**
	 * Verifica a data de validade da crl.
	 * 
	 * @param crl X509CRL
	 * @throws CRLInvalidaException
	 */
	public static void validarDataCRL(X509CRL crl) throws CRLInvalidaException {
		validarDataCRL(crl, new Date());
	}
	
	/**
	 * Verifica a validade da crl numa determinada data.
	 * @param crl X509CRL
	 * @param data Date
	 * @throws CRLInvalidaException
	 */
	public static void validarDataCRL(X509CRL crl, Date data) throws CRLInvalidaException {
		if (crl.getThisUpdate() == null || crl.getNextUpdate() == null) {
			throw new CRLInvalidaException(String.format("A CRL recuperada para [%s] nÃ£o possui um dos campos ThisUpdate ou NextUpdate.", getCrlString(crl)));
		}
		if(isDeltaCRL(crl)){
			throw new CRLInvalidaException("NÃ£o foi possÃ­vel validar delta CRL.");
		}
		// se a data de efetivacao da CRL for superior a data atual ou se a data da proxima atualizacao for inferior a atual
		if (!data.before(crl.getNextUpdate())) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z");
			String thisUpdate = df.format(crl.getThisUpdate());
			String nextUpdate = df.format(crl.getNextUpdate());
			String todayStr = df.format(data);
			throw new CRLInvalidaException(String.format("A CRL recuperada para [%s] nÃ£o pode ser utilizada, pois sÃ³ Ã© valida entre [%s] e [%s]. Data considerada referÃªncia: [%s]", getCrlString(crl), thisUpdate, nextUpdate, todayStr));
		}
	}
	
	/**
	 * Verifica a data de validade de um Certificado. Utiliza a data atual como referÃªncia.
	 * @param cert X509Certificate
	 * @throws CertificadoInvalidoException
	 */
	public static void validarDataCertificado(X509Certificate cert) throws CertificadoInvalidoException {
		validarDataCertificado(cert, null);
	}
	
	/**
	 * Verifica a data de validade de um Certificado numa data de referÃªncia.
	 * @param cert X509Certificate
	 * @param data Date 
	 * @throws CertificadoInvalidoException
	 */
	public static void validarDataCertificado(X509Certificate cert, Date data) throws CertificadoInvalidoException {
		// Valida a data do certificado.
		try {
			if (data == null) {
				cert.checkValidity();
			} else {
				cert.checkValidity(data);
			}
			logger.debug(String.format("Certificado [%s] - checkValidity ok.",getCertString(cert)));
		} catch (CertificateExpiredException e) {
			throw new CertificadoExpiradoException(getCertString(cert),"Certificado expirado.");
		} catch (CertificateNotYetValidException e) {
			throw new CertificadoInvalidoException(getCertString(cert),"Certificado ainda nÃ£o Ã© vÃ¡lido.");
		}
	}

	/**
	 * Recupera a lista de revogaÃ§Ã£o.
	 * 
	 * @param extensionData
	 * @return
	 * @throws IOException
	 */
	public static List<URL> getCdpFromX509Cert(X509Certificate cert) throws IOException {
		List<URL> urls = new LinkedList<URL>();
		byte extensionData[] = cert.getExtensionValue(X509Extensions.CRLDistributionPoints.getId());
		if (extensionData == null) {
			return urls;
		}
		ASN1OctetString string = (ASN1OctetString) getDerObject(extensionData);
		CRLDistPoint dp = new CRLDistPoint((ASN1Sequence) getDerObject(string.getOctets()));
		DistributionPoint[] cdp = dp.getDistributionPoints();

		for (int i = 0; i < cdp.length; i++) {
			DistributionPointName dpn = cdp[i].getDistributionPoint();
			ASN1TaggedObject ato = (ASN1TaggedObject) dpn.toASN1Object();
			if (ato.getTagNo() == 0) {
				GeneralNames gn = GeneralNames.getInstance((ASN1TaggedObject) ato.getDERObject(), false);
				GeneralName[] names = gn.getNames();
				if (names != null) {
					for (int j = 0; j < names.length; j++) {
						if (names[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
							DERIA5String deriaString = ((DERIA5String) names[j].getName());
							urls.add(getUrl(deriaString.getString()));
						}
					}
				}
			}
		}
		return urls;
	}

	/**
	 * Verifica se o certificado passado Ã© folha IcpBrasil.
	 * 
	 * @param cert
	 *            X509Certificate
	 * @return boolean
	 * @throws IOException
	 */
	public static boolean isFolhaIcpBrasil(X509Certificate cert) throws IOException {
		boolean retVal = false;
		byte[] extensaoOID_2_5_29_32 = cert.getExtensionValue("2.5.29.32");
		if (extensaoOID_2_5_29_32 == null)
			return retVal;
		ASN1InputStream oInput = new ASN1InputStream(new ByteArrayInputStream(extensaoOID_2_5_29_32));
		ASN1Sequence certPolicies = null;
		if (oInput == null) {
			return retVal;
		}
		ASN1OctetString oOct = ASN1OctetString.getInstance(oInput.readObject());
		oInput = new ASN1InputStream(new ByteArrayInputStream(oOct.getOctets()));
		certPolicies = ASN1Sequence.getInstance(oInput.readObject());

		for (int i = 0; i < certPolicies.size(); i++) {
			PolicyInformation policy = new PolicyInformation((ASN1Sequence) certPolicies.getObjectAt(i));
			String directive = policy.getPolicyIdentifier().getId();
			if ((((X509Certificate) cert).getBasicConstraints() == -1)
					&& (directive.startsWith("2.16.76.1.2.1") || directive.startsWith("2.16.76.1.2.2") || directive.startsWith("2.16.76.1.2.3")
							|| directive.startsWith("2.16.76.1.2.4") || directive.startsWith("2.16.76.1.2.101") || directive.startsWith("2.16.76.1.2.102")
							|| directive.startsWith("2.16.76.1.2.103") || directive.startsWith("2.16.76.1.2.104"))) {
				retVal = true;
				break;
			}
		}
		return retVal;
	}
	
	/**
	 * Verifica se um certificado Ã© raiz, ou seja, se Ã© auto-assinado.
	 * Um certificado auto-assinado possui o SubjectDn igual ao IssuerDn
	 * 
	 * @param cert X509Certificate
	 * @return boolean
	 */
	public static boolean isRaiz(X509Certificate cert) {
		if (!cert.getSubjectDN().getName().equals(cert.getIssuerDN().getName())) {
			return false;
		}
		return true;
	}

	/**
	 * MÃ©todo responsÃ¡vel por retornar os dados do campo Subject Alternative
	 * Name presente no certificado ou 'null' se alguma exception ocorrer ou
	 * nenhum dado estiver presente. Entre as informaÃ§Ãµes do Subject Alternative
	 * Name estÃ£o o CPF, a OAB, o RG, e outros. Deve ser utilizado apenas para
	 * certificados ICP-Brasil. O CPF estÃ¡ localizado no campo Subject
	 * Alternative Name no OID 2.16.76.1.3.1, no sub-campo OTHERNAME. A
	 * varredura pela busca dos dados Ã© feita de acordo com a especificaÃ§Ã£o da
	 * ICP-Brasil.
	 * 
	 * @return SubjectAlternativeNameICPBrasil contendo o CPF, OAB, RG ou o que
	 *         puder ser recuperado do certificado. Retorna nulo se nÃ£o puder
	 *         ser recuperada nenhuma informaÃ§Ã£o.
	 * @throws AssinadorException
	 *             Caso ocorra algum erro ao recuperar o certificado solicitado.
	 */
	public static SubjectAlternativeNameICPBrasil recuperarSubjectAlternativeNameICPBrasil(X509Certificate certificado) {
		SubjectAlternativeNameICPBrasil san = new SubjectAlternativeNameICPBrasil();
		if (certificado == null) {
			return null;
		}

		// Recupera o nome do titular do certificado.
		String nomeTitular = PdfPKCS7.getSubjectFields(certificado).getField("CN");

		if (nomeTitular != null && !nomeTitular.isEmpty()) {
			int posDoisPontos = nomeTitular.indexOf(':');
			if (posDoisPontos > 0) {
				nomeTitular = nomeTitular.substring(0, posDoisPontos);
			}
		} else {
			return null;
		}
		san.setNomeTitular(nomeTitular);

		// Recupera o campo Subject Alternative Name (nÃ£o crÃ­tico) do
		// certificado.
		byte[] bytesSubjectName = certificado.getExtensionValue(X509Extensions.SubjectAlternativeName.getId());
		if (bytesSubjectName == null) {
			return null;
		}
		ASN1InputStream oInput = new ASN1InputStream(new ByteArrayInputStream(bytesSubjectName));
		ASN1Sequence oSeq = null;
		try {
			ASN1OctetString oOct = ASN1OctetString.getInstance(oInput.readObject());
			oInput = new ASN1InputStream(new ByteArrayInputStream(oOct.getOctets()));
			oSeq = ASN1Sequence.getInstance(oInput.readObject());
		} catch (IOException e) {
		}

		int numOtherNames = 0;
		boolean flagEmail = false;
		// Percorre a sequencia do "Subject Alternative Name"
		for (int i = 0; oSeq != null && i < oSeq.size(); i++) {
			ASN1TaggedObject oTagged = (ASN1TaggedObject) oSeq.getObjectAt(i);

			// Se o campo for "otherName"...
			if (oTagged.getTagNo() == TYPE_OTHERNAME) {
				ASN1Sequence oSeqAgain = DERSequence.getInstance(oTagged.getObject());

				boolean flagOtherName = false;
				for (int j = 0; j < oSeqAgain.size() && !flagOtherName; j++) {
					if (oSeqAgain.getObjectAt(j) instanceof DERObjectIdentifier) {
						DERObjectIdentifier obj = (DERObjectIdentifier) oSeqAgain.getObjectAt(j);

						// Verifica se o campo Ã© do CPF.
						if (obj != null && obj.getId() != null && OID_CPF.equals(obj.getId())) {
							// Trata o CPF achado.
							if (j + 1 < oSeqAgain.size() && oSeqAgain.getObjectAt(j + 1) instanceof DERTaggedObject) {
								ASN1TaggedObject tar = (DERTaggedObject) oSeqAgain.getObjectAt(j + 1);
								String dadosPessoais = null;
								// Como os certificados sÃ£o providos por
								// diferentes ACs, os objetos que contÃ©m o CPF
								// podem diferir entre DERString e
								// ASN1OctetString.
								if (tar.getObject() instanceof ASN1OctetString) {
									ASN1OctetString oCPFData = (ASN1OctetString) tar.getObject();
									if (oCPFData != null) {
										dadosPessoais = new String(oCPFData.getOctets());
									}
								} else {
									DERString oCPFData = (DERString) tar.getObject();
									if (oCPFData != null) {
										dadosPessoais = oCPFData.getString();
									}
								}
								if (dadosPessoais != null && dadosPessoais.length() > 18) {
									san.setCpfCnpj(dadosPessoais.substring(8, 19));
								}
								flagOtherName = true;
								numOtherNames++;
							}
						}
						// TODO: Pegar o numero de CNPJ
						if (obj != null && obj.getId() != null && OID_CNPJ.equals(obj.getId())) {
							// Trata o CNPJ achado.
							if (j + 1 < oSeqAgain.size() && oSeqAgain.getObjectAt(j + 1) instanceof DERTaggedObject) {
								ASN1TaggedObject tar = (DERTaggedObject) oSeqAgain.getObjectAt(j + 1);
								String dadosPessoais = null;
								// Como os certificados sÃ£o providos por
								// diferentes ACs, os objetos que contÃ©m o CNPJ
								// podem diferir entre DERString e
								// ASN1OctetString.
								if (tar.getObject() instanceof ASN1OctetString) {
									ASN1OctetString oCNPJData = (ASN1OctetString) tar.getObject();
									if (oCNPJData != null) {
										dadosPessoais = new String(oCNPJData.getOctets());
									}
								} else {
									DERString oCNPJData = (DERString) tar.getObject();
									if (oCNPJData != null) {
										dadosPessoais = oCNPJData.getString();
									}
								}
								if (dadosPessoais != null) {
									san.setCpfCnpj(dadosPessoais);
								}
								flagOtherName = true;
								numOtherNames++;
							}
						}
						// Verifica se o campo Ã© da OAB.
						else if (obj != null && obj.getId() != null && obj.getId().startsWith(OID_OAB)) {
							if (j + 1 < oSeqAgain.size() && oSeqAgain.getObjectAt(j + 1) instanceof DERTaggedObject) {
								ASN1TaggedObject tar = (DERTaggedObject) oSeqAgain.getObjectAt(j + 1);
								String aobCompleta = null;
								// Como os certificados sÃ£o providos por
								// diferentes ACs, os objetos que contÃ©m o CPF
								// podem diferir entre DERString e
								// ASN1OctetString.
								if (tar.getObject() instanceof ASN1OctetString) {
									ASN1OctetString oCPFData = (ASN1OctetString) tar.getObject();
									if (oCPFData != null) {
										aobCompleta = new String(oCPFData.getOctets());
									}
								} else {
									DERString oCPFData = (DERString) tar.getObject();
									if (oCPFData != null) {
										aobCompleta = oCPFData.getString();
									}
								}
								if (aobCompleta != null && aobCompleta.length() > 8) {
									san.setCodigoOAB(aobCompleta.substring(0, 7));
									san.setUfOAB(aobCompleta.substring(7, 9).toUpperCase());
								}
								flagOtherName = true;
								numOtherNames++;
							}
						}
					}
				}
				// Verifica se o campo Ã© um RFC822Name (e-mail na ICP-Brasil).
			} else if (oTagged.getTagNo() == TYPE_RFC822_NAME) {
				if (oTagged.getObject() instanceof ASN1OctetString) {
					san.setEmail(new String(((ASN1OctetString) oTagged.getObject()).getOctets()));
					flagEmail = true;
				}
			}

			if (numOtherNames > 2 && flagEmail)
				break;
		}

		return san;
	}

	/**
	 * Retorna uma string descritiva do certificado. Utilizada para logs e
	 * mensagens em exceptions.
	 * 
	 * @param cert
	 *            X509Certificate
	 * @return String
	 */
	public static String getCertString(X509Certificate cert) {
		String certString = null;
		if (cert.getSubjectX500Principal() != null) {
			certString = cert.getSubjectX500Principal().getName();
		}
		return certString;
	}
	
	/**
	 * Retorna o Common Name correspondente ao Subject do certificado.
	 * @param cert X509Certificate
	 * @return String
	 * @throws CertificateEncodingException 
	 */
	public static String getCertCn(X509Certificate cert) throws CertificateEncodingException {
		X509Principal principal = PrincipalUtil.getSubjectX509Principal(cert);
		Vector<?> values = principal.getValues(X509Name.CN);
		if (values.size() > 0) {
			return (String) values.get(0);
		}
		return null;
	}

	/**
	 * Retorna uma string descritiva da crl. Utilizada para logs e mensagens em
	 * exceptions.
	 * 
	 * @param crl
	 *            X509CRL
	 * @return String
	 */
	public static String getCrlString(X509CRL crl) {
		if (crl!=null && crl.getIssuerX500Principal() != null) {
			String emissor = crl.getIssuerX500Principal().getName();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z");
			String thisUpdate = df.format(crl.getThisUpdate());
			String nextUpdate = df.format(crl.getNextUpdate());
			String todayStr = df.format(new Date());
			return String.format("Emissor: [%s]. Valida entre [%s] e [%s]. Data considerada atual: [%s]",emissor,thisUpdate,nextUpdate,todayStr);
		}
		return null;
	}

	/**
	 * Trata um array retornando o DERObject correspondente.
	 * 
	 * @param data
	 *            byte[]
	 * @return DERObject
	 * @throws IOException
	 */
	private static DERObject getDerObject(byte data[]) throws IOException {
		ASN1InputStream stream = new ASN1InputStream(data);
		return stream.readObject();
	}

	/**
	 * Converte uma string em um objeto URL.
	 * 
	 * @param value
	 *            String
	 * @return URL
	 * @throws MalformedURLException
	 */
	private static URL getUrl(String value) throws MalformedURLException {
		URL url = new URL(value);
		if ((url.getProtocol().compareToIgnoreCase("http") != 0) && (url.getProtocol().compareToIgnoreCase("https") != 0)) {
			url = null;
		}
		return url;
	}
	
	/**
	 * Converte um certificado para o formato PEM.
	 * @param cert X509Certificate
	 * @return char[]
	 * @throws IOException
	 */
	public static char[] toPem(Object x509Obj) throws IOException{
		if (!(x509Obj instanceof X509Certificate || x509Obj instanceof X509CRL)) {
			throw new IllegalArgumentException("Tipo de argumento invalido. Esperado: X509Certificate ou X509CRL.");
		}
		initBouncycastleProvider();
		CharArrayWriter writer = new CharArrayWriter();
		PEMWriter pemW = new PEMWriter(writer);
		pemW.writeObject(x509Obj);
		pemW.close();
		return writer.toCharArray(); 
	}
	
	/**
	 * Converte um conjunto de caracteres no formato PEM para X509Certificate.
	 * @param charArray
	 * @return X509Certificate
	 * @throws IOException 
	 */
	public static Object fromPem(char[] charArray) throws IOException{
		return fromPem(new CharArrayReader(charArray));
	}
	
	/**
	 * Converte um conjunto de caracteres no formato PEM para X509Certificate.
	 * @param charArray
	 * @return X509Certificate
	 * @throws IOException 
	 */
	public static Object fromPem(Reader reader) throws IOException{
		initBouncycastleProvider();
		PEMReader pemR = new PEMReader(reader);
		Object obj = pemR.readObject();
		pemR.close();
		return obj;
	}
	
	/**
	 * Gera um MD5 check sum para ser utilizado.
	 * @param barray byte[]
	 * @return String
	 * @throws NoSuchAlgorithmException
	 */
	public static String md5Sum(byte[] barray) throws NoSuchAlgorithmException {
		MessageDigest complete = MessageDigest.getInstance("MD5");
		complete.update(barray);
		byte[] digest = complete.digest();
		StringBuilder result = new StringBuilder();
	    for (int i=0; i < digest.length; i++) {
	    	//fast conversion to hex string
	    	result.append(Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 ));
	    }
	    return result.toString();
	}
	
	/**
	 * Gera um sha1 sobre a entrada.
	 * @param barray
	 * @return
	 * @throws NoSuchAlgorithmException 
	 */
	public static byte[] sha1Sum(byte[] barray) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(barray);
		return md.digest();
	}
	
	/**
	 * Verifica se um certificado foi emitido por outro.
	 * 
	 * @param cert
	 *            X509Certificate
	 * @param x509
	 *            X509Certificate
	 * @return boolean
	 */
	public static boolean isIssuer(X509Certificate cert, X509Certificate issuer) {
		try {
			cert.verify(issuer.getPublicKey());
			return true;
		} catch (Exception e) {
			logger.warn("Erro ao verificar issuer do certificado: "+e.getMessage(), e);
		}
		return false;
	}
	
	/**
	 * Verifica se uma crl foi emitida por um certificado.
	 * @param crl X509CRL
	 * @param emissor X509Certificate
	 * @return boolean
	 * @throws CRLInvalidaException
	 */
	public static boolean isIssuer(X509CRL crl, X509Certificate emissor) {
		try {
			crl.verify(emissor.getPublicKey());
			return true;
		} catch (Exception e) {
			logger.warn("Erro ao verificar issuer da CRL: "+e.getMessage(), e);
		}
		return false;
	}
	
	/**
	 * Compara dois certificados x509.
	 * @param a X509Certificate
	 * @param b X509Certificate
	 * @return boolean
	 * @throws IOException
	 */
	public static boolean equals(X509Certificate a, X509Certificate b) throws IOException {
		if(a == b) {
			return true;
		}
		if (a == null) {
			return false;
		}
		if (b == null) {
			return false;
		}
		if(a.equals(b)){
			return true;
		}
		String strA = new String(CertUtil.toPem(a));
		String strB = new String(CertUtil.toPem(b));
		if (strA.equals(strB)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Verifica se uma CRL Ã© delta.
	 * @param crl X509CRL
	 * @return boolean
	 */
    @SuppressWarnings("deprecation")
	public static boolean isDeltaCRL(X509CRL crl) {
        Set<String> criticalExtensions = crl.getCriticalExtensionOIDs();
        return criticalExtensions.contains(X509Extensions.DeltaCRLIndicator.getId());
    }
    
    /**
     * Gera um p7b (PKCS#7) contendo os certificados passados como parÃ¢metro.
     * @param certList List<X509Certificate>
     * @param crlList List<X509CRL>
     * @return byte[]
     */
	@SuppressWarnings("deprecation")
	public static byte[] genP7b(List<X509Certificate> certList, List<X509CRL> crlList) throws IcpRuntimeException {
		try{
			initBouncycastleProvider();
			if (certList == null && crlList == null) {
				throw new IllegalArgumentException("Ã preciso passar um grupo de Certificados ou de Crls para gerar o p7b.");
			}
			CMSSignedDataGenerator signGen = new CMSSignedDataGenerator();
			if (certList != null) {
				CertStore certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
				signGen.addCertificatesAndCRLs(certs);
			}
	
			if (crlList != null) {
				CertStore crlStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(crlList), "BC");
				signGen.addCertificatesAndCRLs(crlStore);
			}
			CMSSignedData signedData = signGen.generate(null, false, "BC");
			byte[] signeddata = signedData.getEncoded();
			return signeddata;
		} catch (InvalidAlgorithmParameterException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		} catch (NoSuchProviderException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		} catch (CMSException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		} catch (IOException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		} catch (CertStoreException e) {
			logger.error("Erro ao gerar p7b: "+e.getMessage(), e);
			throw new IcpRuntimeException("Erro ao gerar p7b: "+e.getMessage(), e);
		}
	}

}
