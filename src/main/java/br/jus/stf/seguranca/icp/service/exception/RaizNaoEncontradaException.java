package br.jus.stf.seguranca.icp.service.exception;

/**
 * Indica que nenhuma raiz confiavel pode ser utilizada.
 * @author Leandro.Oliveira
 *
 */
public class RaizNaoEncontradaException extends SegurancaException {
	private static final long serialVersionUID = 1L;

	/**
	 * Construtor.
	 * @param mensagem String
	 */
	public RaizNaoEncontradaException(String mensagem){
		super(mensagem);
	}

	/**
	 * Construtor. 
	 * @param mensagem String
	 * @param mensagemDetalhada String
	 */
	public RaizNaoEncontradaException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}
	
}
