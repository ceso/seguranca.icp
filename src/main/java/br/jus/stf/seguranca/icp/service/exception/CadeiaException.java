package br.jus.stf.seguranca.icp.service.exception;

public class CadeiaException extends SegurancaException {

	private static final long serialVersionUID = 1L;

	public CadeiaException(String message) {
		super(message);
	}
}
