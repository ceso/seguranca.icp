package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;
import br.jus.stf.seguranca.icp.service.impl.crlmanager.CrlManager;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Utiliza o JCA/JCE SunMSCapi provider para acessar a lista de certificados pessoais do usuÃ¡rio.
 * NÃ£o recupera a lista de CRLs.
 * 
 * @author Leandro.Oliveira
 */
public class KeyStoreServiceSunMsCapiImpl extends KeyStoreServiceTemplate implements KeyStoreService {

	private final Log logger = LogFactory.getLog(KeyStoreServiceSunMsCapiImpl.class);
	private static final String PROVIDER_NAME = "SunMSCAPI";
	private static final String PROVIDER_CLASS = "sun.security.mscapi.SunMSCAPI";
	/**
	 * Mapa que contÃ©m o dn da autoridade certificadora raiz e como chave o serial.
	 */
	private Map<String, String> autoridadesRaizes;
	/**
	 * Armazena as autoridades raÃ­zes que sÃ£o montadas sempre que Ã© solicitada
	 * uma cadeia para um certificado. Feito assim, por motivos de performance.
	 */
	private List<X509Certificate> autoridadesCerts = new LinkedList<X509Certificate>();
	
	private CrlManager crlManager;

	/**
	 * Construtor.
	 * Inicializa o crlManager com 5 threads, 18s para timeouts. 
	 */
	public KeyStoreServiceSunMsCapiImpl() {
		this.crlManager = new CrlManager(5, 180000, 18000, 5000);
	}
	
	/**
	 * Construtor.
	 * @param connectionTimeout int timeout de conexÃ£o em millis.
	 * @param waitForPacketTimeout int timeout de pacotes em millis.
	 */
	public KeyStoreServiceSunMsCapiImpl(int connectionTimeout, int waitForPacketTimeout, long defaultCrlWaitTime){
		this.crlManager = new CrlManager(5, connectionTimeout, waitForPacketTimeout, defaultCrlWaitTime);
	}
	
	/**
	 * Carrega o provider.
	 */
	private void initSunMsCapiProvider() {
		if (Security.getProvider(PROVIDER_NAME) == null) {
			try {
				Provider sunMSCAPIProvider = (Provider) Class.forName(PROVIDER_CLASS).newInstance();
				Security.addProvider(sunMSCAPIProvider);
			} catch (Exception e) {
				logger.warn(e);
				throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
			}
		}
	}
	
	/**
	 * Retorna algum keyStore para o provider.
	 * @param type String
	 * @return KeyStore
	 */
	private KeyStore getKeyStore(String type){
		try {
			KeyStore ks = KeyStore.getInstance(type, PROVIDER_NAME);
			ks.load(null, null);
			return ks;
		} catch (KeyStoreException e) {
			throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
		} catch (NoSuchProviderException e) {
			throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
		} catch (NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
		} catch (CertificateException e) {
			throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
		} catch (IOException e) {
			throw new UnsupportedOperationException("NÃ£o foi possÃ­vel carregar o driver SunMSCAPI.");
		}
	}
	
	/**
	 * Retorna aliases existentes no keystore do tipo windows-my.
	 * ContÃ©m certificados pessoais do usuÃ¡rio.
	 * @return
	 */
	public List<String> windowsMyAliases() {
		initSunMsCapiProvider();
		KeyStore ksWindowsMy = getKeyStore("Windows-My");
		try {
			LinkedList<String> aliases = new LinkedList<String>();
			Enumeration<String> aliasesWindowsMy = ksWindowsMy.aliases();
			while (aliasesWindowsMy.hasMoreElements()) {
				aliases.add(aliasesWindowsMy.nextElement());
			}
			return aliases;
		} catch (KeyStoreException e) {
			throw new UnsupportedOperationException(e);
		}
	}
	
	/**
	 * Retorna aliases existentes no keystore do tipo windows-root.
	 * ContÃ©m certificados pessoais do usuÃ¡rio.
	 * @return
	 */
	public List<String> windowsRootAliases() {
		initSunMsCapiProvider();
		KeyStore ksWindowsRoot = getKeyStore("Windows-Root");
		try {
			LinkedList<String> aliases = new LinkedList<String>();
			Enumeration<String> aliasesWindowsRoot = ksWindowsRoot.aliases();
			while (aliasesWindowsRoot.hasMoreElements()) {
				aliases.add(aliasesWindowsRoot.nextElement());
			}
			return aliases;
		} catch (KeyStoreException e) {
			throw new UnsupportedOperationException(e);
		}
	}
	
	/**
	 * Retorna os aliases existentes no windows-my e no windows-root.
	 */
	public List<String> aliases() {
		initSunMsCapiProvider();
		List<String> aliases = windowsMyAliases();
		aliases.addAll(windowsRootAliases());
		return aliases;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateByAlias(java.lang.String)
	 */
	public X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException {
		initSunMsCapiProvider();
		try {
			KeyStore ksWindowsRoot = getKeyStore("Windows-Root");
			if (ksWindowsRoot.containsAlias(alias)) {
				Certificate cert = ksWindowsRoot.getCertificate(alias);
				if (cert instanceof X509Certificate) {
					return (X509Certificate) cert;
				}
			}
			KeyStore ksWindowsMy = getKeyStore("Windows-My");
			if (ksWindowsMy.containsAlias(alias)) {
				Certificate cert = ksWindowsMy.getCertificate(alias);
				if (cert instanceof X509Certificate) {
					return (X509Certificate) cert;
				}
			}
		} catch (KeyStoreException e) {
			throw new UnsupportedOperationException(e);
		}
		throw new UnsupportedOperationException(String.format("Certificado nÃ£o encontrado pelo SunMsCapi, alias: [%].", alias));
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.impl.KeyStoreServiceTemplate#getCertificateChain(java.security.cert.X509Certificate)
	 */
	public List<X509Certificate> getCertificateChain(X509Certificate cert) throws CertificadoInvalidoException {
		try {
			KeyStore ksWindowsRoot = getKeyStore("Windows-Root");
			String alias = ksWindowsRoot.getCertificateAlias(cert);
			Certificate chain[]  = null;
			// verifica se o certificado estÃ¡ cadastrado no windows-root
			if( alias !=null) {
				// recupera a chain do keystore
				chain = ksWindowsRoot.getCertificateChain(alias);
			} else {
				KeyStore ksWindowsMy = getKeyStore("Windows-My");
				alias = ksWindowsMy.getCertificateAlias(cert);
				// verifica se o certificado estÃ¡ cadastrado no windows-my
				if (alias != null) {
					// recupera a chain do keystore
					chain = ksWindowsMy.getCertificateChain(alias);
				}
			}
			if (chain == null) {
				throw new UnsupportedOperationException("NÃ£o foi possÃ­vel recuperar a cadeia pelo SunMSCapi.");
			}
			List<X509Certificate> cadeia = new LinkedList<X509Certificate>();
			for (int i = 0; i < chain.length; i++) {
				if (chain[i] instanceof X509Certificate) {
					X509Certificate x509Certificate = (X509Certificate) chain[i];
					cadeia.add(x509Certificate);
					verificarCertificadoRaiz(x509Certificate);
				} else {
					throw new UnsupportedOperationException("Um dos certificados que fazem parte da cadeia nÃ£o implementa X509Certificate.");
				}
			}
			return cadeia;
		} catch (KeyStoreException e) {
			throw new UnsupportedOperationException(e);
		}
	}
	
	/**
	 * Verifica se o certificado confere com o de alguma raiz.
	 * Se sim, adiciona Ã  lista de trusted anchors.
	 * HÃ¡ a possibilidade de injetar um certificado no cliente que tenha os mesmos atributos de um certificado raiz icp-brasil,
	 * no entanto, a validaÃ§Ã£o que existe na entrada de documentos e tambÃ©m no prÃ³prio login no peticionamento impediria uma fraude
	 * (que inclusive poderia ser feita com qualquer outro assinador).
	 * TODO estudar outra soluÃ§Ã£o.
	 * @param x509Certificate
	 */
	private void verificarCertificadoRaiz(X509Certificate x509Certificate) {
		//se ainda nÃ£o estiver cadastrado
		if (!getTrustedAnchors().contains(x509Certificate)) {
			Set<Entry<String, String>> entrySet = autoridadesRaizes.entrySet();
			for (Entry<String, String> entry : entrySet) {
				// testes para verificar se um dos itens confere com
				// uma raiz confiavel
				if (new BigInteger(entry.getValue()).equals(x509Certificate.getSerialNumber())) {
					if (entry.getKey().equals(x509Certificate.getSubjectX500Principal().getName())) {
						addTrustedAnchor(x509Certificate);
					}
				}
			}
		}
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.impl.KeyStoreServiceTemplate#getTrustedAnchors()
	 */
	public synchronized List<X509Certificate> getTrustedAnchors() {
		return this.autoridadesCerts;
	}

	/**
	 * Adiciona uma autoridade na lista de confiÃ¡veis.
	 * Esta lista Ã© montada dinamicamente, sempre que Ã© necessÃ¡rio recuperar uma cadeia,
	 * por motivos de performance.
	 * @param cert X509Certificate
	 */
	private synchronized void addTrustedAnchor(X509Certificate cert){
		this.autoridadesCerts.add(cert);
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateBySubjectName(java.lang.String)
	 */
	public X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException {
		throw new UnsupportedOperationException(String.format("Certificado nÃ£o encontrado pelo SunMsCapi [%s].", subjectDn));
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate, java.util.Date)
	 */
	public X509CRL getCrl(X509Certificate cert, Date data) throws CRLInvalidaException {
		X509CRL crl = crlManager.getCrlByIssuer(cert);
		try {
			// se nao for folha, (ou seja, deveria recuperar a crl)
			if (crl == null && !CertUtil.isFolhaIcpBrasil(cert)) {
				logger.info("Falha ao recuperar CRL localmente. Caso existam problemas, favor entrar em contato com o suporte.");
				throw new UnsupportedOperationException(String.format("NÃ£o foi possÃ­vel recuperar a CRL para [%s].", CertUtil.getCertString(cert)));
			} else {
				if(crl!=null) {
					logger.info("CRL recuperada localmente.");
					CertUtil.validarDataCRL(crl, data);
				}
				return crl;
			}
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		}
	}
	
	/**
	 * Adiciona os certificados da cadeia e espera pelo download.
	 * @see br.jus.stf.seguranca.icp.service.impl.KeyStoreServiceTemplate#getCrlList(java.util.List)
	 */
	@Override
	public List<X509CRL> getCrlList(List<X509Certificate> chain) throws CRLInvalidaException {
		// adiciona os certificados ao crl manager
		for (X509Certificate cert : chain) {
			try {
				// espera pelo download
				crlManager.addCertificadoAndWait(cert);
			} catch (IOException e) {
				logger.error(e);
				throw new UnsupportedOperationException(e);
			} catch (InterruptedException e) {
				logger.error(e);
				throw new UnsupportedOperationException(e);
			}
		}
		return super.getCrlList(chain);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getPrivateKey(java.lang.String, java.lang.String)
	 */
	public PrivateKey getPrivateKey(String alias, String password) {
		if (password == null) {
			password = "";
		}
		initSunMsCapiProvider();
		KeyStore ksWindowsMy = getKeyStore("Windows-My");
		KeyStore ksWindowsRoot = getKeyStore("Windows-Root");
		try {
			if (ksWindowsMy.containsAlias(alias)) {
				Key key = ksWindowsMy.getKey(alias, password.toCharArray());
				if (key != null && key instanceof PrivateKey) {
					return (PrivateKey) key;
				}
			}
			if (ksWindowsRoot.containsAlias(alias)) {
				Key key = ksWindowsRoot.getKey(alias, password.toCharArray());
				if (key != null && key instanceof PrivateKey) {
					return (PrivateKey) key;
				}
			}
		} catch (KeyStoreException e) {
			throw new IcpRuntimeException("Falha ao recuperar chave privada.", e);
		} catch (UnrecoverableKeyException e) {
			throw new IcpRuntimeException("Falha ao recuperar chave privada.", e);
		} catch (NoSuchAlgorithmException e) {
			throw new IcpRuntimeException("Falha ao recuperar chave privada.", e);
		}
		throw new UnsupportedOperationException("NÃ£o foi possÃ­vel recuperar a chave privada.");
	}

	public Map<String, String> getAutoridadesRaizes() {
		return autoridadesRaizes;
	}

	public void setAutoridadesRaizes(Map<String, String> autoridadesRaizes) {
		this.autoridadesRaizes = autoridadesRaizes;
	}
}
