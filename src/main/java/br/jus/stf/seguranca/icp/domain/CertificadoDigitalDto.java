package br.jus.stf.seguranca.icp.domain;

import java.security.cert.X509Certificate;
import java.util.Date;

public class CertificadoDigitalDto {

	private String serial;
	private String descricao;
	private String descricaoResumida;
	private Date inicioValidade;
	private Date fimValidade;
	private boolean valido;
	private boolean cadastradoStf;
	private String erro;
	private String erroDetalhado;
	private X509Certificate cert;

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricaoResumida() {
		return descricaoResumida;
	}

	public void setDescricaoResumida(String descricaoResumida) {
		this.descricaoResumida = descricaoResumida;
	}

	public Date getInicioValidade() {
		return inicioValidade;
	}

	public void setInicioValidade(Date inicioValidade) {
		this.inicioValidade = inicioValidade;
	}

	public Date getFimValidade() {
		return fimValidade;
	}

	public void setFimValidade(Date fimValidade) {
		this.fimValidade = fimValidade;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getErroDetalhado() {
		return erroDetalhado;
	}

	public void setErroDetalhado(String erroDetalhado) {
		this.erroDetalhado = erroDetalhado;
	}

	public X509Certificate getCert() {
		return cert;
	}

	public void setCert(X509Certificate cert) {
		this.cert = cert;
	}

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public boolean isCadastradoStf() {
		return cadastradoStf;
	}

	public void setCadastradoStf(boolean cadastradoStf) {
		this.cadastradoStf = cadastradoStf;
	}

}
