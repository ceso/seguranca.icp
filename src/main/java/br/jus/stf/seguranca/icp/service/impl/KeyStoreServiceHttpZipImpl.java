package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.cache.CacheResponseStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;

/**
 * Recupera CAs e CRLs disponíveis em arquivos zip por meio do protocolo HTTP.
 *  
 * @author Leandro.Oliveira
 */
public class KeyStoreServiceHttpZipImpl extends KeyStoreServiceTemplate {
	
	private final Log logger = LogFactory.getLog(KeyStoreServiceHttpZipImpl.class);
	private Ehcache cacheCertificados;
	private Ehcache cacheCrls;
	private String urlCasZip;
	private String urlCrlsZip;
	private HttpClient httpClient;
	
	/**
	 * Construtor.
	 */
	public KeyStoreServiceHttpZipImpl(){
		initHttpClient();
		initEhcache();
	}
	
	/**
	 * Cria o objeto HttpClient
	 */
	private void initHttpClient() {
		// Cria o http client 
		DefaultHttpClient tempClient = new DefaultHttpClient();
		// Tempo para abrir uma conexão.
		tempClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
        // Timeout para intervalo de pacotes.
		tempClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
        // Some tuning that is not required for bit tests.
		tempClient.getParams().setParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);
		tempClient.getParams().setParameter(CoreConnectionPNames.TCP_NODELAY, true);
        // Configura a cache, 3 objetos, 1MB para cada
		CacheConfig cacheConfig = new CacheConfig();  
		cacheConfig.setMaxCacheEntries(3);
		cacheConfig.setMaxObjectSizeBytes(1048576);
		//realizar 3 tentativas
		tempClient.setHttpRequestRetryHandler(new RequestRetryHandler());
        httpClient = new CachingHttpClient(new CachingHttpClient(tempClient, cacheConfig));
	}
	
	/**
	 * Cria o objeto EhCache.
	 */
	private void initEhcache(){
		CacheManager cacheManager = CacheManager.create(getClass().getResourceAsStream("/br/jus/stf/seguranca/icp/service/impl/http-zip-ehcache.xml"));
		cacheCertificados = cacheManager.getCache("cacheCertificados");
		cacheCrls = cacheManager.getEhcache("cacheCrls");
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#aliases(br.jus.stf.seguranca.icp.service.KeyStoreContext)
	 */
	public List<String> aliases() {
		throw new UnsupportedOperationException("Não existem aliases recuperando por Http.");
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateByAlias(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String)
	 */
	public X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException {
		throw new UnsupportedOperationException("Não existem aliases recuperando por Http.");
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateBySubjectName(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String)
	 */
	public X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException {
		try {
			return getCertificados().get(subjectDn);
		} catch (ClientProtocolException e) {
			throw new UnsupportedOperationException("Falha ao recuperar zip de CAs.",e);
		} catch (IOException e) {
			throw new UnsupportedOperationException("Falha ao recuperar zip de CAs.",e);
		}
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate, java.util.Date)
	 */
	public X509CRL getCrl(X509Certificate cert, Date data) throws CRLInvalidaException {
		try {
			return getCrls().get(cert);
		} catch (ClientProtocolException e) {
			throw new UnsupportedOperationException("Falha ao recuperar zip de CRLs.",e);
		} catch (IOException e) {
			throw new UnsupportedOperationException("Falha ao recuperar zip de CRLs.",e);
		}
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getPrivateKey(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String, java.lang.String)
	 */
	public PrivateKey getPrivateKey(String alias, String password) {
		throw new UnsupportedOperationException("Não é possível recuperar a chave privada por http.");
	}
	
	/**
	 * Recupera da cache ou carrega por download.
	 * @return 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, X509Certificate> getCertificados() throws ClientProtocolException, IOException {
		Element element = cacheCertificados.get("certificados");
		if (element != null) {
			logger.debug("Cache-hit para CAs.");
			return (LinkedHashMap<String, X509Certificate>) element.getValue();
		}
		logger.debug("Cache-miss para CAs.");
		LinkedHashMap<String, X509Certificate> downloaded = downloadCas();
		if (downloaded != null) {
			cacheCertificados.put(new Element("certificados", downloaded));
		}
		return downloaded;
	}


	/**
	 * Recupera da cache ou carrega por download.
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public LinkedHashMap<X509Certificate, X509CRL> getCrls() throws ClientProtocolException, IOException {
		Element element = cacheCrls.get("crls");
		if (element != null) {
			logger.debug("Cache-hit para CRL.");
			return (LinkedHashMap<X509Certificate, X509CRL>) element.getValue();
		}
		logger.debug("Cache-miss para CRL.");
		LinkedHashMap<X509Certificate, X509CRL> downloaded = downloadCrls(getCertificados());
		if (downloaded != null) {
			cacheCrls.put(new Element("crls", downloaded));
		}
		return downloaded;
	}
	
	/**
	 * Faz o download.
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private LinkedHashMap<String, X509Certificate> downloadCas() throws ClientProtocolException, IOException {
		LinkedHashMap<String, X509Certificate> cas = null;
		// busca CAs
		try {
			logger.info("Download de CAs. URL: "+getUrlCasZip());
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGetCas = new HttpGet(getUrlCasZip());
			cas = httpClient.execute(httpGetCas, new CaZipRespHandler(),localContext);
			CacheResponseStatus responseStatus = (CacheResponseStatus) localContext.getAttribute(CachingHttpClient.CACHE_RESPONSE_STATUS);
			logCacheStatus(responseStatus);
		} finally {
			if (cas != null) {
				return cas;
			}
		}
		return null;
	}
	
	/**
	 * Faz o download.
	 * @param cas
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	private LinkedHashMap<X509Certificate, X509CRL> downloadCrls(LinkedHashMap<String, X509Certificate> cas) throws ClientProtocolException, IOException {
		if(cas==null){
			throw new NullPointerException("Obrigatorio ter uma lista de CAs para montar a lista de CRLs.");
		}
		LinkedHashMap<X509Certificate, X509CRL> crls = null;
		//busca CRLs
		try {
			logger.info("Download de CRLs. URL: "+getUrlCrlsZip());
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGetCrl = new HttpGet(getUrlCrlsZip());
			crls = httpClient.execute(httpGetCrl, new CrlZipRespHandler(cas),localContext);
			CacheResponseStatus responseStatus = (CacheResponseStatus) localContext.getAttribute(CachingHttpClient.CACHE_RESPONSE_STATUS);
			logCacheStatus(responseStatus);
		} finally {
			if (crls != null) {
				return crls;
			}
		}
		return null;
	}
	
	/**
	 * Gera informações de log sobre a cache.
	 * 
	 * @param responseStatus
	 */
	private void logCacheStatus(CacheResponseStatus responseStatus) {
		switch (responseStatus) {
		case CACHE_HIT:
			logger.debug("Cache status: Cache-hit. Nenhuma requisicao feita ao servidor.");
			break;
		case CACHE_MODULE_RESPONSE:
			logger.debug("Cache status: Cache-Module-Response. Resposta gerada pelo caching module.");
			break;
		case CACHE_MISS:
			logger.debug("Cache status: Cache-Miss. Foi necessário acessar o servidor.");
			break;
		case VALIDATED:
			logger.debug("Cache status: Cache-Validated. Foi necessário validar com o servidor de origem, mas obtida pela cache.");
			break;
		}
	}	

	public String getUrlCasZip() {
		return urlCasZip;
	}

	public void setUrlCasZip(String urlCasZip) {
		this.urlCasZip = urlCasZip;
	}

	public String getUrlCrlsZip() {
		return urlCrlsZip;
	}

	public void setUrlCrlsZip(String urlCrlsZip) {
		this.urlCrlsZip = urlCrlsZip;
	}
}
