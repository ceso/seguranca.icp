package br.jus.stf.seguranca.icp.service.exception;

/**
 * Marca erros na carimbadora.
 * 
 * @author Leandro.Oliveira
 */
public class CarimbadoraException extends SegurancaException {
	/**
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Construtor.
	 * @param mensagem String
	 */
	public CarimbadoraException(String mensagem){
		super(mensagem);
	}

	/**
	 * Construtor. 
	 * @param mensagem String
	 * @param mensagemDetalhada String
	 */
	public CarimbadoraException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}
}
