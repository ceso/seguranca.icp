package br.jus.stf.seguranca.icp.service;

import java.security.PrivateKey;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;

/**
 * Representa um repositÃ³rio de certificados e crls.
 * 
 * @author Leandro.Oliveira
 */
public interface KeyStoreService {

	/**
	 * Recupera todos os aliases dos objetos do repositÃ³rio.
	 * @return
	 */
	List<String> aliases();
	
	/**
	 * Recupera a chave privada.
	 * @param alias
	 * @param password
	 * @return
	 */
	PrivateKey getPrivateKey(String alias, String password);
	
	/**
	 * Recupera um certificado por seu alias.
	 * @param alias
	 * @return
	 * @throws CertificadoInvalidoException
	 */
	X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException;
	
	/**
	 * Recupera um certificado pelo subject name.
	 * @param subjectDn
	 * @return
	 * @throws CertificadoInvalidoException
	 */
	X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException;
	
	/**
	 * 
	 * @param cert
	 * @return
	 * @throws CertificadoInvalidoException
	 * @throws CadeiaException
	 */
	List<X509Certificate> getCertificateChain(X509Certificate cert) throws CertificadoInvalidoException, CadeiaException;
	
	/**
	 * Recupera a crl. Valida com base na data atual.
	 * @param cert
	 * @return
	 * @throws CRLInvalidaException
	 */
	X509CRL getCrl(X509Certificate cert) throws CRLInvalidaException;
	
	/**
	 * Recupera a crl. Valida com base na data passada como argumento.
	 * @param cert X509Certificate
	 * @param date Date
	 * @return X509CRL
	 * @throws CRLInvalidaException
	 */
	X509CRL getCrl(X509Certificate cert, Date date) throws CRLInvalidaException;

	/**
	 * Recupera as crls. Valida cada uma com base na data atual.
	 * @param chain
	 * @return
	 * @throws CRLInvalidaException
	 */
	List<X509CRL> getCrlList(List<X509Certificate> chain) throws CRLInvalidaException;
	
	/**
	 * Recupera as crls. Valida cada uma com base na data passada como argumento.
	 * @param chain
	 * @param data
	 * @return
	 * @throws CRLInvalidaException
	 */
	List<X509CRL> getCrlList(List<X509Certificate> chain, Date data) throws CRLInvalidaException;

	/**
	 * Recupera as ancoras confiÃ¡veis cadastradas neste repositÃ³rio.
	 * @return
	 */
	List<X509Certificate> getTrustedAnchors();
}
