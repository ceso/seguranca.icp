package br.jus.stf.seguranca.icp.service.exception;


/**
 * Identifica erros no certificado.
 * @author Leandro.Oliveira
 *
 */
public class CertificadoInvalidoException extends SegurancaException{

	private static final long serialVersionUID = 1L;

	/**
	 * Construtor.
	 * @param mensagem String
	 */
	public CertificadoInvalidoException(String mensagem){
		super(mensagem);
	}

	/**
	 * Construtor. 
	 * @param mensagem String
	 * @param mensagemDetalhada String
	 */
	public CertificadoInvalidoException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}
	
}
