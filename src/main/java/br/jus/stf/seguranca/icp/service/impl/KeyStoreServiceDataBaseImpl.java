package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.domain.Crl;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;
import br.jus.stf.seguranca.icp.util.CertUtil;
/**
 * Acessa o banco de dados para recuperar certificados.
 * NÃ£o possui implementaÃ§Ã£o para mÃ©todos que retornam a chave privada.
 *  
 * @author Leandro.Oliveira
 */
@Repository
public class KeyStoreServiceDataBaseImpl extends KeyStoreServiceTemplate {
	
	private CadastroCertificadoService cadastroService;
	private final Log logger = LogFactory.getLog(KeyStoreServiceDataBaseImpl.class);

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#aliases(br.jus.stf.seguranca.icp.service.KeyStoreContext)
	 */
	public List<String> aliases() {
		// recupera as raizes e autoridades certificadoras
		List<CertificadoDigital> list = cadastroService.findAncorasConfiaveis();
		list.addAll(cadastroService.findIntermediarias());
		LinkedList<String> aliases = new LinkedList<String>();
		for (CertificadoDigital certificadoDigital : list) {
			String alias = certificadoDigital.getDescricao();
			aliases.add(alias);
		}
		return aliases;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateByAlias(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String)
	 */
	public X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException {
		return getCertificateBySubjectName(alias);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateBySubjectName(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String)
	 */
	public X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException  {
		List<CertificadoDigital> resultList = cadastroService.findCertificadoDigital(subjectDn);
		for (CertificadoDigital certificado : resultList) {
			try {
				X509Certificate x509 = certificado.getX509();
				if (x509 == null) {
					continue;
				}
				//CertUtil.validarDataCertificado(x509);
				return x509;
			} catch (IOException e) {
				throw new IcpRuntimeException(String.format("Erro ao recuperar o certificado [%s].", subjectDn), e);
			}
		}
		return null;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate, java.util.Date)
	 */
	public X509CRL getCrl(X509Certificate cert, Date data) throws CRLInvalidaException {
		
		CertificadoDigital certDigital = cadastroService.findCertificadoDigital(cert);
		if (certDigital == null) {
			logger.warn(String.format("Certificado nÃ£o cadastrado na base [%s].", CertUtil.getCertString(cert)));
			return null;
		}
		Crl crl = cadastroService.findLatestCrl(certDigital);
		if (crl != null) {
			try {
				X509CRL x509 = crl.getX509();
				if (x509 != null) {
					CertUtil.validarDataCRL(x509, data);
				}
				return x509;
			} catch (IOException e) {
				throw new IcpRuntimeException(String.format("Erro ao recuperar a crl do certificado [%s].", CertUtil.getCertString(cert)), e);
			}
		}
		return null;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getPrivateKey(br.jus.stf.seguranca.icp.service.KeyStoreContext, java.lang.String, java.lang.String)
	 */
	public PrivateKey getPrivateKey(String alias, String password) {
		throw new UnsupportedOperationException();
	}

	public CadastroCertificadoService getCadastroService() {
		return cadastroService;
	}

	public void setCadastroService(CadastroCertificadoService cadastroService) {
		this.cadastroService = cadastroService;
	}

}
