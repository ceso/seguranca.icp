package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;

import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

/**
 * Tenta executar a requisição por 3 vezes.
 * 
 * @author Leandro.Oliveira
 */
public class RequestRetryHandler implements HttpRequestRetryHandler {

	public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
		if (executionCount >= 3) {
            return false;
        }
		return true;
	}

}
