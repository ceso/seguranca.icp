package br.jus.stf.seguranca.icp.service.exception;

/**
 * Marca o erro de um certificado vencido.
 * 
 * @author Leandro.Oliveira
 */
public class CertificadoExpiradoException extends CertificadoInvalidoException {

	private static final long serialVersionUID = 1L;

	/**
	 * Construtor.
	 * @param mensagem
	 */
	public CertificadoExpiradoException(String mensagem) {
		super(mensagem);
	}
	
	/**
	 * Construtor.
	 * @param mensagem
	 * @param mensagemDetalhada
	 */
	public CertificadoExpiradoException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}

}
