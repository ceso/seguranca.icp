package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;


/**
 * Recupera da requisição a lista de CAs.
 * 
 * @author Leandro.Oliveira
 */
public class CaZipRespHandler implements ResponseHandler<LinkedHashMap<String, X509Certificate>> {

	private final Log logger = LogFactory.getLog(KeyStoreServiceHttpZipImpl.class);

	/**
	 * Recupera a lista de certificados da requisição.
	 */
	public LinkedHashMap<String, X509Certificate> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		StatusLine status = response.getStatusLine();
		// se status diferente de 200
		if (status.getStatusCode() != HttpStatus.SC_OK) {
			// gera erro
			throw new HttpResponseException(status.getStatusCode(), response.getStatusLine().getReasonPhrase());
		} else {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream entityContent = null;
				try {
					entityContent = entity.getContent();
					ZipInputStream zipInputStream = new ZipInputStream(entityContent);
					// monta a lista de CAs
					return populateCa(zipInputStream);
				} catch (CertificateException e) {
					logger.error("Erro ao popular lista de CAs.",e);
					throw new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
				} finally {
					if (entityContent != null) {
						entityContent.close();
					}
				}
			}
		}
		return null;
	}

	/**
	 * Monta a lista de CAs a partir de um zip.
	 * 
	 * @param zip
	 *            ZipFile
	 * @return LinkedHashMap<String, X509Certificate>
	 * @throws CertificateException
	 * @throws IOException
	 */
	private LinkedHashMap<String, X509Certificate> populateCa(ZipInputStream zipIn) throws CertificateException, IOException {
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		LinkedHashMap<String, X509Certificate> temp = new LinkedHashMap<String, X509Certificate>();
		ZipEntry ze;
		while ((ze = zipIn.getNextEntry()) != null) {
			logger.debug(String.format("CA recebida: %s tamanho %d.", ze.getName(), ze.getSize()));
			X509Certificate cert = (X509Certificate) factory.generateCertificate(zipIn);
			temp.put(cert.getSubjectX500Principal().getName(), cert);
		}
		return temp;
	}
}
