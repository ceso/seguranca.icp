package br.jus.stf.seguranca.icp.domain;

import java.io.IOException;
import java.io.Serializable;
import java.security.cert.X509CRL;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Representa as listas de revogaÃ§Ã£o de certificados baixadas atravÃ©s de uma determinada url existente em
 * um ponto de distribuiÃ§Ã£o.
 * CRLs vencidas podem ser encontradas no CAS atravÃ©s do hash.
 * 
 * @author Leandro.Oliveira
 */
@Entity
@Table(schema = "CORP", name = "LISTA_CERTIFICADO_REVOGADO")
public class Crl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Date inicioValidade;
	private Date fimValidade;
	private char[] crl;
	private CertificadoDigital certificado;
	
	@Id
	@Column(name = "SEQ_LISTA_CERTIFICADO_REVOGADO")
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", sequenceName = "CORP.SEQ_LISTA_CERTIFICADO_REVOGADO", allocationSize=0)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "DAT_VALIDADE_INICIAL")
	public Date getInicioValidade() {
		return inicioValidade;
	}
	public void setInicioValidade(Date inicioValidade) {
		this.inicioValidade = inicioValidade;
	}
	
	@Column(name = "DAT_VALIDADE_FINAL")
	public Date getFimValidade() {
		return fimValidade;
	}
	public void setFimValidade(Date fimValidade) {
		this.fimValidade = fimValidade;
	}
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "TXT_LISTA")
	public char[] getCrl() {
		return crl;
	}
	public void setCrl(char[] crl) {
		this.crl = crl;
	}
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "SEQ_CERTIFICADO_DIGITAL", referencedColumnName = "SEQ_CERTIFICADO_DIGITAL")
	public CertificadoDigital getCertificado() {
		return certificado;
	}

	public void setCertificado(CertificadoDigital certificado) {
		this.certificado = certificado;
	}
	
	@Transient
	public X509CRL getX509() throws IOException {
		if (getCrl() == null || getCrl().length == 0) {
			return null;
		}
		return (X509CRL) CertUtil.fromPem(getCrl());
	}
	
	@Transient
	public boolean isValida() throws IOException{
		X509CRL crl = getX509();
		if (crl == null) {
			return false;
		}
		try {
			CertUtil.validarDataCRL(crl);
			return true;
		} catch (CRLInvalidaException e) {
			return false;
		}
	}
}
