package br.jus.stf.seguranca.icp.service;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Date;

import br.jus.stf.seguranca.icp.domain.CertificadoDigitalDto;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.DocumentoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.RaizNaoEncontradaException;
import br.jus.stf.seguranca.icp.service.exception.SegurancaException;

/**
 * Define operaÃ§Ãµes relacionadas Ã  infra-estrutura de chaves pÃºblicas.
 * Um IcpService utiliza um KeyStoreService, que pode ser uma implementaÃ§Ã£o
 * capaz de acessar keyStores em PKCS12, PKCS11, SunMSCAPI ou mais de um entre eles,
 * com o padrÃ£o Strategy.
 * 
 * 
 * @author leandro.oliveira
 *
 */
public interface IcpService {
	
	/**
	 * Valida a Assinatura de um documento.
	 * @param pdf File
	 * @throws SegurancaException
	 * @throws CadeiaException 
	 */
	void validarAssinatura(File pdf) throws SegurancaException, CadeiaException;
	
	/**
	 * Valida a assinatura de um documento.
	 * @param pdf
	 * @throws SegurancaException
	 * @throws CadeiaException 
	 */
	void validarAssinatura(InputStream pdf) throws SegurancaException, CadeiaException;
	
	/**
	 * Assina um documento.
	 * @param pdfIn InputStream
	 * @param pdfOut OutputStream
	 * @param cert X509Certificate
	 * @param privateKey PrivateKey
	 * @throws DocumentoInvalidoException
	 * @throws CRLInvalidaException
	 * @throws CertificadoInvalidoException
	 * @throws RaizNaoEncontradaException 
	 * @throws CadeiaException 
	 */
	void assinarPdf(InputStream pdfIn, OutputStream pdfOut,X509Certificate cert, PrivateKey privateKey) throws DocumentoInvalidoException, CRLInvalidaException, CertificadoInvalidoException, RaizNaoEncontradaException, CadeiaException;
	
	/**
	 * Valida o certificado. Faz validaÃ§Ã£o de cadeia montada com base no cadastro de ACs do STF. 
	 * Verifica se estÃ¡ vÃ¡lido e nÃ£o revogado na data atual.
	 * 
	 * @param cert Certificate
	 * @throws CertificadoInvalidoException
	 * @throws CRLInvalidaException
	 * @throws RaizNaoEncontradaException 
	 * @throws CadeiaException 
	 */
	void validarCertificado(X509Certificate cert) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException;
	
	
	/**
	 * Valida o certificado. Faz validaÃ§Ã£o de cadeia montada com base no cadastro de ACs do STF.
	 * Verifica se estÃ¡ vÃ¡lido e nÃ£o revogado na data passada como argumento.
	 * 
	 * @param cert Certificate
	 * @param data Date
	 * @throws CertificadoInvalidoException
	 * @throws CRLInvalidaException
	 * @throws RaizNaoEncontradaException 
	 * @throws CadeiaException 
	 */
	void validarCertificado(X509Certificate cert, Date data) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException;
	
	/**
	 * Valida o certificado. Faz validaÃ§Ã£o de cadeia com base no array passado como argumento.
	 * Verifica se estÃ¡ vÃ¡lido e nÃ£o revogado na data atual.
	 * 
	 * @param chain X509Certificate[] 
	 * @param crlValidation boolean
	 * @throws CertificadoInvalidoException
	 * @throws CRLInvalidaException
	 * @throws RaizNaoEncontradaException
	 * @throws CadeiaException
	 */
	void validarCertificado(X509Certificate chain[], boolean crlValidation) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException;
	
	
	/**
	 * Valida o certificado. Faz validaÃ§Ã£o de cadeia com base no array passado como argumento.
	 * Verifica se estÃ¡ vÃ¡lido e nÃ£o revogado na data passada como argumento.
	 * 
	 * @param chain X509Certificate[] 
	 * @param data Date
	 * @param crlValidation boolean
	 * @throws CertificadoInvalidoException
	 * @throws CRLInvalidaException
	 * @throws RaizNaoEncontradaException
	 * @throws CadeiaException
	 */
	void validarCertificado(X509Certificate chain[], Date data, boolean crlValidation) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException;
	
	/**
	 * Valida o certificado digital.
	 * Retorna um objeto de validaÃ§Ã£o com detalhes.
	 * @param cert Certificate
	 * @return CertificadoDigitalDto
	 */
	CertificadoDigitalDto detalharCertificado(X509Certificate cert);
	
	/**
	 * Retorna o KeyStoreService que estÃ¡ sendo utilizado.
	 * NecessÃ¡rio para que o cliente deste serviÃ§o possa apresentar mensagens de erro ou
	 * identificar a estratÃ©gia que foi utilizada para o serviÃ§o de assinatura.
	 * @return
	 */
	KeyStoreService getKeyStoreService();
}
