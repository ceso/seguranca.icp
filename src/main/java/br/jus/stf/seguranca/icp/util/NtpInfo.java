package br.jus.stf.seguranca.icp.util;

import java.util.Calendar;

/**
 * Dados de tempo.
 * 
 * @author Leandro.Oliveira
 */
public class NtpInfo {
	/** Deslocamento em millis entre cliente e servidor. */
	private double offsetMillis;
	/** CalendÃ¡rio de referÃªncia. */ 
	private Calendar referenceCalendar;
	
	public double getOffsetMillis() {
		return offsetMillis;
	}
	public void setOffsetMillis(double offsetMillis) {
		this.offsetMillis = offsetMillis;
	}
	public Calendar getReferenceCalendar() {
		return referenceCalendar;
	}
	public void setReferenceCalendar(Calendar referenceCalendar) {
		this.referenceCalendar = referenceCalendar;
	}
}
