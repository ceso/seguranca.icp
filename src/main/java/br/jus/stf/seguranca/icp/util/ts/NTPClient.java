package br.jus.stf.seguranca.icp.util.ts;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;


public class NTPClient
{
	public static Calendar getTimestampNTP(String NTPserver, int port) throws UnknownHostException{
		Calendar retorno = null;
		try{
			DatagramSocket socket = new DatagramSocket();
			InetAddress address = InetAddress.getByName(NTPserver);
			byte[] buf = new NtpMessage().toByteArray();
			DatagramPacket packet =
				new DatagramPacket(buf, buf.length, address, port);
			
			// Set the transmit timestamp *just* before sending the packet
			// ToDo: Does this actually improve performance or not?
			NtpMessage.encodeTimestamp(packet.getData(), 40,
				(System.currentTimeMillis()/1000.0) + 2208988800.0);

			socket.send(packet);
			
			// Get response
			System.out.println("NTP request sent, waiting for response...\n");
			packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);
			
			// Process response
			NtpMessage msg = new NtpMessage(packet.getData());
			
			retorno = NtpMessage.timestampToCalendar(msg.referenceTimestamp);
		}catch(Throwable t) //Em caso de erro utilizará horário local.
		{
			System.out.println("Erro"+t.getLocalizedMessage()+"/n pegando hora local.");
			retorno = Calendar.getInstance();
			
		}
		
		return 	retorno;
	}
	
	public static void main(String[] args) throws IOException
	{
		
		String serverName = "ntpserver.stf.jus.br";
		System.out.println(getTimestampNTP(serverName,123));

	}
}

