package br.jus.stf.seguranca.icp.service;

import java.io.IOException;
import java.net.URL;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import java.util.List;

import br.jus.stf.seguranca.icp.domain.AssinaturaDigital;
import br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora;
import br.jus.stf.seguranca.icp.domain.CentroDistribuicaoCRL;
import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.domain.Crl;
import br.jus.stf.seguranca.icp.domain.TipoCertificadoDigital;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;

/**
 * Service responsável por manter o cadastro de certificados digitais na base do STF.
 * 
 * @author Leandro.Oliveira
 */
@SuppressWarnings("deprecation")
public interface CadastroCertificadoService {
	
	/**
	 * Recupera as âncoras confiáveis cadastradas no banco de dados.
	 * @return List<CertificadoDigital>
	 */
	List<CertificadoDigital> findAncorasConfiaveis();
	
	/**
	 * Recuperar as autoridades intermediárias.
	 * @return List<CertificadoDigital>
	 */
	List<CertificadoDigital> findIntermediarias();
	
	/**
	 * Recupera certificados digitais cujo emissor e tipo são equivalentes ao passado como argumentos.
	 * @param emissor CertificadoDigital
	 * @param tipoFilho TipoCertificadoDigital
	 * @return List<CertificadoDigital>
	 */
	List<CertificadoDigital> findEmitidosPor(CertificadoDigital emissor, TipoCertificadoDigital tipoFilho);
	
	/**
	 * Recupera certificados com o mesmo subjectDn.
	 *  
	 * @param subjectDn String
	 * @return List<CertificadoDigital>
	 */
	List<CertificadoDigital> findCertificadoDigital(String subjectDn);
	
	/**
	 * Retorna o CertificadoDigital cadastrado para o X509 ou null se não estiver cadastrado.
	 * 
	 * @param cert X509Certificate
	 * @return CertificadoDigital
	 */
	CertificadoDigital findCertificadoDigital(X509Certificate cert);
	
	/**
	 * Retorna o CertificadoDigital cadastrado com o id especificado.
	 * @param id Long
	 * @return CertificadoDigital
	 */
	CertificadoDigital findCertificadoDigital(Long id);
	
	/**
	 * Retorna a lista de certificados digitais cadastrados para um usuário.
	 * @param idUsuario Long
	 * @return List<CertificadoDigital> 
	 */
	List<CertificadoDigital> findCertificadosDigitaisUsuario(Long idUsuario);
	
	
	/**
	 * Recupera todas os certificados digitais cadastrados de usuário final.
	 * @deprecated Os métodos relacionados à classe CertificadoDigital devem ser utilizados.
	 * @return List<AssinaturaDigital>
	 */
	List<AssinaturaDigital> findAllAssinaturaDigital();
	
	/**
	 * Retorna a autoridade certificadora cadastrada para o X509 ou null se não estiver cadastrada.
	 * @param cert
	 * @deprecated
	 * @return AutoridadeCertificadora
	 */
	AutoridadeCertificadora findAutoridadeCertificadora(X509Certificate cert);
	
	/**
	 * Retorna o X509Certificate correspondente ao issuer do certificado ou null se não estiver cadastrado.
	 * @param cert
	 * @return
	 */
	X509Certificate findIssuer(X509Certificate cert);
	
	/**
	 * Recupera a cadeia para um X509.
	 * @param cert X509Certificate
	 * @return List<X509Certificate>
	 * @throws CadeiaException 
	 */
	List<X509Certificate> findChain(X509Certificate cert) throws CadeiaException;
	
	/**
	 * Recupera centros de distribuicao.
	 */
	CentroDistribuicaoCRL findCentroDistribuicao(URL url, CertificadoDigital issuer);
	
	/**
	 * Recupera a ultima CRL baixada para um certificado digital.
	 * @param issuer CertificadoDigital
	 * @return Crl
	 */
	Crl findLatestCrl(CertificadoDigital issuer);
	
	/**
	 * Insere ou atualiza o CertificadoDigital e sua cadeia.
	 * @param certConvertido CertificadoDigital
	 */
	void saveOrUpdate(CertificadoDigital certConvertido);
	
	
	/**
	 * Converte uma cadeia em uma cadeia de certificados digitais convertidos e
	 * atualizados com as informações necessárias para persistir, certificado convertido não é persistido na base.
	 * 
	 * @param chain
	 * @return
	 * @throws IOException
	 * @throws CertificateEncodingException
	 */
	CertificadoDigital convertChain(List<X509Certificate> chain) throws CertificateEncodingException, IOException;
	
	/**
	 * Insere ou atualiza a CRL.
	 * @param crl Crl
	 */
	void saveOrUpdate(Crl crl);

	/**
	 * Recupera a lista de autoridades certificadoras.
	 * @return List<AutoridadeCertificadora>
	 */
	List<AutoridadeCertificadora> findAllAutoridadeCertificadora();
}
