package br.jus.stf.seguranca.icp.service.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * Encapsula excecoes que nao puderam ser tratadas pelo ICPService.
 * 
 * 
 * @author Leandro.Oliveira
 */
public class IcpRuntimeException extends NestedRuntimeException {

	public IcpRuntimeException(String msg) {
		super(msg);
	}

	public IcpRuntimeException(String msg, Throwable cause) {
		super(msg, cause);
	}

	private static final long serialVersionUID = 1L;

}
