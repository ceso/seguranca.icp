package br.jus.stf.seguranca.icp.util;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

/**
 * Wrapper para X509.
 * NecessÃ¡rio para passar na validaÃ§Ã£o do itext, que nÃ£o utiliza um CertPathValidator para avaliar
 * extensÃµes crÃ­ticas.
 * @author Leandro.Oliveira
 *
 */
public class X509CertificateWrapper extends X509Certificate{
	
	private X509Certificate wrapped;
	private Boolean noUnsupportedCriticalExtension = null;
	
	/**
	 * Construtor. Indica o certificado para o qual serÃ£o direcionadas as invocaÃ§Ãµes.
	 * 
	 * @param wrapped X509Certificate
	 * @param noUnsupportedCriticalExtension
	 */
	public X509CertificateWrapper(X509Certificate wrapped, Boolean noUnsupportedCriticalExtension){
		this.wrapped = wrapped;
		this.noUnsupportedCriticalExtension = noUnsupportedCriticalExtension;
	}

	@Override
	public void checkValidity() throws java.security.cert.CertificateExpiredException, java.security.cert.CertificateNotYetValidException {
		wrapped.checkValidity();
	}

	@Override
	public void checkValidity(Date date) throws java.security.cert.CertificateExpiredException, java.security.cert.CertificateNotYetValidException {
		wrapped.checkValidity(date);
	}

	@Override
	public int getBasicConstraints() {
		return wrapped.getBasicConstraints();
	}

	@Override
	public List<String> getExtendedKeyUsage() throws CertificateParsingException {
		return wrapped.getExtendedKeyUsage();
	}

	@Override
	public Collection<List<?>> getIssuerAlternativeNames() throws CertificateParsingException {
		return wrapped.getIssuerAlternativeNames();
	}

	@Override
	public Principal getIssuerDN() {
		return wrapped.getIssuerDN();
	}

	@Override
	public boolean[] getIssuerUniqueID() {
		return wrapped.getIssuerUniqueID();
	}

	@Override
	public X500Principal getIssuerX500Principal() {
		return wrapped.getIssuerX500Principal();
	}

	@Override
	public boolean[] getKeyUsage() {
		return wrapped.getKeyUsage();
	}

	@Override
	public Date getNotAfter() {
		return wrapped.getNotAfter();
	}

	@Override
	public Date getNotBefore() {
		return wrapped.getNotBefore();
	}

	@Override
	public BigInteger getSerialNumber() {
		return wrapped.getSerialNumber();
	}

	@Override
	public String getSigAlgName() {
		return wrapped.getSigAlgName();
	}

	@Override
	public String getSigAlgOID() {
		return wrapped.getSigAlgOID();
	}

	@Override
	public byte[] getSigAlgParams() {
		return wrapped.getSigAlgParams();
	}

	@Override
	public byte[] getSignature() {
		return wrapped.getSignature();
	}

	@Override
	public Collection<List<?>> getSubjectAlternativeNames() throws CertificateParsingException {
		return wrapped.getSubjectAlternativeNames();
	}

	@Override
	public Principal getSubjectDN() {
		return wrapped.getSubjectDN();
	}

	@Override
	public boolean[] getSubjectUniqueID() {
		return wrapped.getSubjectUniqueID();
	}

	@Override
	public X500Principal getSubjectX500Principal() {
		return wrapped.getSubjectX500Principal();
	}

	@Override
	public byte[] getTBSCertificate() throws java.security.cert.CertificateEncodingException {
		return wrapped.getTBSCertificate();
	}

	@Override
	public int getVersion() {
		return wrapped.getVersion();
	}

	public Set<String> getCriticalExtensionOIDs() {
		return wrapped.getCriticalExtensionOIDs();
	}

	public byte[] getExtensionValue(String oid) {
		return wrapped.getExtensionValue(oid);
	}

	public Set<String> getNonCriticalExtensionOIDs() {
		return wrapped.getNonCriticalExtensionOIDs();
	}

	/**
	 * Indica que nÃ£o existem extensÃµes crÃ­ticas nÃ£o suportadas.
	 */
	public boolean hasUnsupportedCriticalExtension() {
		if(Boolean.TRUE.equals(noUnsupportedCriticalExtension)){
			return false;
		}
		return wrapped.hasUnsupportedCriticalExtension();
	}

	@Override
	public boolean equals(Object other) {
		return wrapped.equals(other);
	}

	@Override
	public byte[] getEncoded() throws java.security.cert.CertificateEncodingException {
		return wrapped.getEncoded();
	}

	@Override
	public PublicKey getPublicKey() {
		return wrapped.getPublicKey();
	}

	@Override
	public int hashCode() {
		return wrapped.hashCode();
	}

	@Override
	public String toString() {
		return wrapped.toString();
	}

	@Override
	public void verify(PublicKey key, String sigProvider) throws java.security.cert.CertificateException, NoSuchAlgorithmException, InvalidKeyException,
			NoSuchProviderException, SignatureException {
		wrapped.verify(key, sigProvider);
	}

	@Override
	public void verify(PublicKey key) throws java.security.cert.CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException,
			SignatureException {
		wrapped.verify(key);
	}

}
