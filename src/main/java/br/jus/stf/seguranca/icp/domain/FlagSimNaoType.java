package br.jus.stf.seguranca.icp.domain;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

/**
 * Tipo do hibernate para converter o texto S ou N para Boolean.
 * 
 * @author Leandro.Oliveira
 */
public class FlagSimNaoType implements UserType, Serializable {

	private static final long serialVersionUID = 1L;

	private static final int[] SQL_TYPES = { Types.VARCHAR };

	private static final String INDEFINIDO = "X";
	private static final String SIM = "S";
	private static final String NAO = "N";

	public FlagSimNaoType() {

	}

	public int[] sqlTypes() {

		// /Hibernate.CHAR_ARRAY.sqlType();

		return SQL_TYPES;
	}

	@SuppressWarnings(value = "unchecked")
	public Class returnedClass() {
		return Boolean.class;
	}

	public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {

		String flag = resultSet.getString(names[0]);
		if (flag != null) {
			flag = flag.toUpperCase();
		}

		Boolean result = null;

		if (!resultSet.wasNull()) {
			if (flag != null && flag.equals(SIM))
				result = Boolean.TRUE;
			else if (flag != null && flag.equals(NAO))
				result = Boolean.FALSE;
			else if (flag != null && flag.equals(INDEFINIDO))
				result = Boolean.FALSE;
		}

		return result;
	}

	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index) throws HibernateException, SQLException {

		if ((value == null) || !(value instanceof Boolean)) {
			preparedStatement.setNull(index, Types.VARCHAR);
		} else {
			String flag = NAO;
			Boolean valor = (Boolean) value;

			if (valor.booleanValue())
				flag = SIM;

			preparedStatement.setString(index, flag);
		}
	}

	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	public boolean isMutable() {
		return false;
	}

	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y)
			return true;
		if (null == x || null == y)
			return false;
		return x.equals(y);
	}
}