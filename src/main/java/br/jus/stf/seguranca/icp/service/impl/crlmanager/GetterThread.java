package br.jus.stf.seguranca.icp.service.impl.crlmanager;

import java.net.URL;
import java.security.cert.X509CRL;
import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Objeto runnable que recupera CRLs atravÃ©s de um HttpClient.
 * O Objeto HttpClient deve estar preparado pra poder executar diversas requisiÃ§Ãµes simultÃ¢neas.
 * 
 * 
 * @author Leandro.Oliveira
 */
public class GetterThread implements Runnable {

	private final Log logger = LogFactory.getLog(GetterThread.class);
	/** Objeto em que serÃ¡ armazenada a CRL baixada. */
	private CrlDownloadManager crlDownloadManager;
	/** URL para download. */
	private URL url;
	/** Cliente para requisiÃ§Ãµes. */
	private HttpClient httpClient;
	/** Executor de threads. Utilizado para que este Thread 
	 * gerencie seu prÃ³prio agendamento com base na situaÃ§Ã£o da CRL Baixada ou nÃ£o.*/
	private ScheduledExecutorService executor;
	/** PerÃ­odo padrÃ£o para agendar o thread. Por padrÃ£o, este Thread rodarÃ¡ o mais rÃ¡pido possÃ­vel, quando nenhuma CRL for baixada.
	 *  Se nÃ£o puder ser recuperada nenhuma CRL, este Thread farÃ¡ o auto agendamento para rodar em no mÃ¡ximo defaultSchedulePeriod (padrÃ£o Ã© 2 min)
	 *  minutos apÃ³s ter rodado.
	 */
	private int defaultSchedulePeriod = 2;
	/**
	 * PerÃ­odo padrÃ£o para agendar o thread de download de CRLs para certificados expirados.
	 */
	private int defaultExpiredSchedulePeriod = 30;

	/**
	 * Construtor.
	 * @param crlDownloadManager CrlDownloadManager
	 * @param url URL
	 * @param executor Executor
	 */
	public GetterThread(CrlDownloadManager crlDownloadManager, URL url, HttpClient client, ScheduledExecutorService executor) {
		this.crlDownloadManager = crlDownloadManager;
		this.url = url;
		this.httpClient = client;
		this.executor = executor;
	}

	/**
	 * Faz o download da CRL.
	 */
	public void run() {
		try {
			HttpContext context = new BasicHttpContext();
			HttpGet httpGet = new HttpGet(url.toExternalForm());
			// baixa a crl
			X509CRL crl = httpClient.execute(httpGet, new X509CrlResponseHandler(), context);
			if (crl != null) {
				crlDownloadManager.setCrl(crl, url);
			}
		} catch (Exception e) {
			logger.error("Falha ao processar URL: "+url+" erro: "+e.getMessage(),e);
			crlDownloadManager.setException(url, e);
		} finally {
			int nextRun = getNextRunPeriod();
			logger.debug("URL "+url+" agendada para rodar em "+nextRun+" min.");
			executor.schedule(this, nextRun, TimeUnit.MINUTES);
		}
	}
	
	/**
	 * Retorna o prÃ³ximo minuto em que rodarÃ¡ o download da CRL.
	 * @return
	 */
	private int getNextRunPeriod() {
		int minutesToExpire = getMinutesToExpire();
		// se jÃ¡ estÃ¡ expirada
		if (minutesToExpire <= 0) {
			// se o certificado estÃ¡ expirado
			if(isCertExpired()) {
				// roda no tempo de certificados expirados
				return defaultExpiredSchedulePeriod;
			}
			// roda no tempo mÃ­nimo padrÃ£o
			return defaultSchedulePeriod;
		} else {
			// rodarÃ¡ na metade do tempo para expirar, contanto que nÃ£o seja menor q o tempo mÃ­nimo
			int period = minutesToExpire / 2;
			if (period < defaultSchedulePeriod) {
				return defaultSchedulePeriod;
			}
			return period;
		}
	}
	
	/**
	 * Indica se o certificado cujas CRLs seriam baixadas esta expirado.
	 * @return boolean
	 */
	private boolean isCertExpired() {
		// Se o certificado para o qual deve ser baixada a CRL estiver vencido
		if (!crlDownloadManager.isCdpsFromCertificate()) {
			// Se nao foi possivel baixar a crl
			try {
				CertUtil.validarDataCertificado(crlDownloadManager.getCertificate());
			} catch (CertificadoInvalidoException e) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Retorna o tempo em minutos para que a CRL expire. Ou negativo, se jÃ¡
	 * tiver expirado.
	 * 
	 * @return
	 */
	private int getMinutesToExpire() {
		if (!crlDownloadManager.isCrlValida()) {
			return -1;
		}
		Date nextUpdate = crlDownloadManager.getCrlDto().getCrl().getNextUpdate();
		Date now = new Date();
		int diffFromNow = getMinutesBetween(nextUpdate, now);
		return (int) diffFromNow;
	}
	
	/**
	 * Retorna a diferenÃ§a em minutos entre duas datas.
	 * @param d1 Date
	 * @param d2 Date
	 * @return int
	 */
	private int getMinutesBetween(Date d1, Date d2){
		// diferenca em minutos
		long diff = (d1.getTime() - d2.getTime()) / 1000 / 60;
		return (int) diff;
	}

	public URL getUrl() {
		return url;
	}
}
