package br.jus.stf.seguranca.icp.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.CRL;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.jus.stf.seguranca.icp.domain.CertificadoDigitalDto;
import br.jus.stf.seguranca.icp.service.IcpService;
import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.exception.AssinaturaInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoExpiradoException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.DocumentoInvalidoException;
import br.jus.stf.seguranca.icp.service.exception.DocumentoParcialmenteAssinadoException;
import br.jus.stf.seguranca.icp.service.exception.IcpRuntimeException;
import br.jus.stf.seguranca.icp.service.exception.RaizNaoEncontradaException;
import br.jus.stf.seguranca.icp.service.exception.SegurancaException;
import br.jus.stf.seguranca.icp.util.CertUtil;
import br.jus.stf.seguranca.icp.util.IcpBrasilCriticalExtensionsChecker;
import br.jus.stf.seguranca.icp.util.X509CertificateWrapper;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * Implementa os serviços de Pki.
 * Classe com código refatorado dos serviços existentes no barramento, componentes: stfcertvalidator, sspjassinador-core, stfchainstore .
 * 
 * 
 * @author Leandro.Oliveira
 * @author Alberto.Soares (autor dos componentes: stfcertvalidator, sspjassinador-core, stfchainstore )
 */
public class IcpServiceImpl implements IcpService {

	private KeyStoreService keyStoreService;
	private final Log logger = LogFactory.getLog(IcpService.class);
	private boolean aceitarAssinaturaCertificadoVencido = false;
	private boolean aceitarUmaAssinaturaValida = false;
	private boolean aceitarAssinaturaSemCadeia = false;


	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarAssinatura(java.io.File)
	 */
	public void validarAssinatura(File pdf) throws SegurancaException, CadeiaException {
		FileInputStream fileIn = null;
		try {
			//verifica a assinatura
			fileIn = new FileInputStream(pdf);
			validarAssinatura(fileIn);
		} catch (FileNotFoundException e) {
			throw new IcpRuntimeException("Arquivo não existe e não pode ser enviado para verificação de assinatura.", e);
		} finally {
			if (fileIn != null) {
				try {
					fileIn.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarAssinatura(java.io.InputStream)
	 */
	public void validarAssinatura(InputStream pdf) throws SegurancaException, CadeiaException {
		PdfReader reader;
		try {
			reader = new PdfReader(pdf);
		} catch (IOException e) {
			throw new IcpRuntimeException("Falha ao abrir o pdf.",e);
		}
		// Recupera os campos das assinaturas do pdf.
		AcroFields acroFields = reader.getAcroFields();
		if (acroFields == null) {
			throw new AssinaturaInvalidaException("Não foi possível recuperar os campos de assinatura do pdf.");
		}
		ArrayList<String> names = acroFields.getSignatureNames();
		if (names == null || names.size() < 1) {
			throw new AssinaturaInvalidaException("Não foi possível recuperar os campos de assinatura do pdf.");
		}
		int qtdAssinaturasValidas = 0;
		StringBuffer mensagemErroDetalhado = new StringBuffer();
		//para cada campo de assinatura
		for (String nomeCampo : names) {
			try {
				// valida o campo de assinatura
				validarAcroFieldAssinatura(acroFields, nomeCampo);
				qtdAssinaturasValidas++;
			} catch(SegurancaException e) {
				// se configurado para validar todas as assinaturas, lanca o erro imediatamente
				if(!aceitarUmaAssinaturaValida) {
					throw e;
				}
				mensagemErroDetalhado.append("Falha ao validar campo de assinatura: "+(e.getMensagemDetalhada()!=null?e.getMensagemDetalhada():e.getMensagem()));
				mensagemErroDetalhado.append(". ");
			}
		}
		// verifica se encontrou alguma valida
		if(qtdAssinaturasValidas==0) {
			throw new SegurancaException("Nenhuma assinatura existente no documento foi considerada válida.", mensagemErroDetalhado.toString());
		}
	}

	private void validarAcroFieldAssinatura(AcroFields acroFields,String nomeCampo) throws AssinaturaInvalidaException, CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException {
		try {
			//recupera um PdfPKCS7 para continuar a verificação
			PdfPKCS7 pdfPKCS7 = acroFields.verifySignature(nomeCampo, CertUtil.PROVIDER_BC_NAME);
			if (pdfPKCS7 == null) {
				throw new AssinaturaInvalidaException("Assinatura não encontrada no pdf.");
			}
			//verifica o digesto da assinatura
			if (!pdfPKCS7.verify()) {
				throw new AssinaturaInvalidaException("O digesto da assinatura não confere.");
			}
			//verifica se a assinatura compreende o documento todo
			if (!acroFields.signatureCoversWholeDocument(nomeCampo)){
				throw new DocumentoParcialmenteAssinadoException("A assinatura não compreende o documento todo.");
			}
			
			try {
				// valida se o certificado está válido atualmente
				validarCertificado(pdfPKCS7.getSigningCertificate());
			} catch (CertificadoExpiradoException e) {
				// se nao aceita assinatura com certificado vencido
				if (!this.aceitarAssinaturaCertificadoVencido) {
					// relanca a exception
					throw e;
				}
			}
			//TODO implementar validacao de timestamp
			X509Certificate[] chain = (X509Certificate[]) pdfPKCS7.getSignCertificateChain();
			if ((chain.length == 0 || chain.length == 1) && this.aceitarAssinaturaSemCadeia) {
				// Recupera a cadeia do certificado.
				List<X509Certificate> cadeia = this.keyStoreService.getCertificateChain(pdfPKCS7.getSigningCertificate());
				chain = cadeia.toArray(new X509Certificate[] {});
			}
			
			Collection<CRL> crl = pdfPKCS7.getCRLs();
			if(crl == null) {
				// Recupera a lista de revogação dos certificados.
				//crl = new LinkedList<CRL>(this.keyStoreService.getCrlList(Arrays.asList(chain)));
				//TODO verificar erro de validacao quando passa a crl vinda do banco
			}
			
			// TODO remover este código em versao futura
			for (int i = 0; i < chain.length; i++) {
				if (chain[i].hasUnsupportedCriticalExtension()) {
					// gera um wrapper para passar sem verificar extensões críticas
					// que já são feitas acima por validarCertificado(pdfPKCS7.getSigningCertificate())
					chain[i] = new X509CertificateWrapper(chain[i], true);
				}
			}
			
			Object fails[] = PdfPKCS7.verifyCertificates(chain, getTrustedAnchorsStore(), crl, pdfPKCS7.getSignDate());
			if (fails != null) {
				//monta a mensagem de erro
				throw new CertificadoInvalidoException("Erro na cadeia de certificação: "+fails[0], fails[1] != null ? fails[1].toString() : null);
			}
		} catch (SignatureException e) {
			throw new IcpRuntimeException("Falha na assinatura.",e);
		}
	}
	
	/**
	 * Retorna um keystore inicializado com as ancoras confiaveis.
	 */
	private KeyStore getTrustedAnchorsStore() {
		try {
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			try {
				//inicializa o keystore
				ks.load(null, null);
			} catch (NoSuchAlgorithmException e) {
				throw new IcpRuntimeException("Erro gerar keystore para validar o caminho de certificacao.",e);
			} catch (CertificateException e) {
				throw new IcpRuntimeException("Erro gerar keystore para validar o caminho de certificacao.",e);
			} catch (IOException e) {
				throw new IcpRuntimeException("Erro gerar keystore para validar o caminho de certificacao.",e);
			}
			List<X509Certificate> acsConfiaveis = this.keyStoreService.getTrustedAnchors();
			for (X509Certificate certConfiavel : acsConfiaveis) {
				try {
					// Nao utiliza ancoras vencidas ou nao validas
					certConfiavel.checkValidity();
					String md5Sum = CertUtil.md5Sum(certConfiavel.getEncoded());
					String alias = CertUtil.getCertCn(certConfiavel);
					alias += md5Sum.substring(0, 10);
					alias += certConfiavel.getSerialNumber().toString();
					ks.setCertificateEntry(alias, certConfiavel);
				} catch (CertificateExpiredException e) {
					continue;
				} catch (CertificateNotYetValidException e) {
					continue;
				} catch (CertificateEncodingException e) {
					throw new IcpRuntimeException("Falha ao montar keystore com âncoras confiáveis.",e);
				} catch (NoSuchAlgorithmException e) {
					throw new IcpRuntimeException("Falha ao montar keystore com âncoras confiáveis.",e);
				}
			}
			return ks;
		} catch (KeyStoreException e) {
			throw new IcpRuntimeException("Falha no keystore",e);
		}
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarCertificado(java.security.cert.Certificate)
	 */
	public void validarCertificado(X509Certificate cert) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException {
		validarCertificado(new X509Certificate[]{cert}, null, true);
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarCertificado(java.security.cert.Certificate, java.util.Date)
	 */
	public void validarCertificado(X509Certificate cert, Date date) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException {
		validarCertificado(new X509Certificate[]{cert}, date, true);
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarCertificado(java.security.cert.X509Certificate[], boolean)
	 */
	public void validarCertificado(X509Certificate[] chain, boolean crlValidation) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException {
		validarCertificado(chain, null, crlValidation);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.IcpService#validarCertificado(java.security.cert.X509Certificate[], java.util.Date)
	 */
	public void validarCertificado(X509Certificate[] chain, Date date, boolean crlValidation) throws CertificadoInvalidoException, CRLInvalidaException, RaizNaoEncontradaException, CadeiaException {
		CertUtil.initBouncycastleProvider();

		X509Certificate x509Cert = chain[0];

		// Verifica se o certificado está habilitado para assinatura digital.
		if (x509Cert.getKeyUsage() == null || (!x509Cert.getKeyUsage()[0] && !x509Cert.getKeyUsage()[1])) {
			throw new CertificadoInvalidoException("Certificado não é habilitado para assinatura:" + getCertString(x509Cert),"Certificado não é habilitado para assinatura.");
		}

		// Recupera a cadeia do certificado.
		List<X509Certificate> cadeia;
		if(chain.length==1) {
			// Recupera a cadeia a partir da base do STF
			cadeia = this.keyStoreService.getCertificateChain(x509Cert);
		} else {
			cadeia = Arrays.asList(chain);
		}
		
		try {
			// constroi os parametros de validacao
			PKIXParameters pkixParameters = buildPkixParameters(cadeia, date, crlValidation);

			CertPath certPath = CertificateFactory.getInstance("X.509", CertUtil.PROVIDER_BC_NAME).generateCertPath(cadeia);
			CertPathValidator validador = CertPathValidator.getInstance("PKIX", CertUtil.PROVIDER_BC_NAME);
			// executa a validacao com os parametros informados
			validador.validate(certPath, pkixParameters);
			// valida as datas do certificado (expirado ou ainda nao valido para uso).
			CertUtil.validarDataCertificado(x509Cert, date);
		} catch (CertificateException e) {
			throw new CertificadoInvalidoException("Erro do certificado: "+getCertString(x509Cert), e.getMessage());
		} catch (NoSuchProviderException e) {
			throw new IcpRuntimeException("Erro de provider ao validar o certificado:"+CertUtil.getCertString(x509Cert), e);
		} catch (CertPathValidatorException e) {
			throw new CertificadoInvalidoException("Falha na validação da cadeia do certificado:"+getCertString(x509Cert),e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			throw new IcpRuntimeException("Erro algoritmo invalido ao validar o certificado:"+CertUtil.getCertString(x509Cert), e);
		} catch (NoSuchAlgorithmException e) {
			throw new IcpRuntimeException("Erro algoritmo invalido ao validar o certificado:"+CertUtil.getCertString(x509Cert), e);
		}
	}

	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.IcpService#detalharCertificado(java.security.cert.Certificate)
	 */
	public CertificadoDigitalDto detalharCertificado(X509Certificate cert) {
		CertificadoDigitalDto dto = new CertificadoDigitalDto();
		try {
			dto.setCert(cert);
			dto.setSerial(cert.getSerialNumber().toString());
			dto.setValido(false);
			
			dto.setDescricao(CertUtil.getCertString(cert));
			dto.setDescricaoResumida(CertUtil.getCertCn(cert));
			
			dto.setInicioValidade(cert.getNotBefore());
			dto.setFimValidade(cert.getNotAfter());
			
			validarCertificado(cert);
			dto.setValido(true);
			//TODO verificar se o certificado está cadastrado.
		} catch (SegurancaException e) {
			dto.setErro(e.getMensagem());
			dto.setErroDetalhado(e.getMensagemDetalhada());
		} catch (CertificateEncodingException e) {
			dto.setErro("Erro de encoding.");
			dto.setErroDetalhado(e.getMessage());
		}
		return dto;
	}

	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.IcpService#assinarPdf(java.io.InputStream, java.io.OutputStream, java.security.cert.X509Certificate, java.security.PrivateKey)
	 */
	public void assinarPdf(InputStream pdfIn, OutputStream pdfOut, X509Certificate cert, PrivateKey privateKey) throws DocumentoInvalidoException, CRLInvalidaException, CertificadoInvalidoException, RaizNaoEncontradaException, CadeiaException {
		
		this.validarCertificado(cert);
		List<X509Certificate> chain = this.keyStoreService.getCertificateChain(cert);
		List<X509CRL> crls = this.keyStoreService.getCrlList(chain);
		
		// Abre uma abstração iText do pdf original a ser copiado no momento da assinatura.
		PdfReader pdfOriginal;
		try {
			pdfOriginal = new PdfReader(pdfIn);
		} catch (IOException e) {
			throw new IcpRuntimeException("Impossivel abrir pdf para assinar.", e);
		}
		
		// Verifica se o arquivo pdf original está criptografado e não permite cópia.
		if (pdfOriginal.isEncrypted()) {
			throw new DocumentoInvalidoException("O documento passado para ser assinado está criptografado e não pode ser assinado.");
		}

		PdfStamper pdfStamper;
		try {
			//cria estampa do tipo assinatura
			pdfStamper = PdfStamper.createSignature(pdfOriginal, pdfOut, '\0');
		} catch (DocumentException e) {
			throw new IcpRuntimeException("Não foi possível criar assinatura", e);
		} catch (IOException e) {
			throw new IcpRuntimeException("Não foi possível criar assinatura", e);
		}
		// Retira assinaturas prévias se houver.
		List<String> signatureNames = pdfStamper.getAcroFields().getSignatureNames();
		for (String signatureName : signatureNames) {
			pdfStamper.getAcroFields().removeField(signatureName);	
		}
		// Configura as informações da assinatura.
		PdfSignatureAppearance signatureAppearance = pdfStamper.getSignatureAppearance();
		signatureAppearance.setCrypto(privateKey, chain.toArray(new Certificate[]{}), crls.toArray(new CRL[]{}), PdfSignatureAppearance.WINCER_SIGNED);
		Calendar today = Calendar.getInstance();
		signatureAppearance.setSignDate(today);
		StringBuffer layer2Text = new StringBuffer("Assinado por ");
		layer2Text.append(PdfPKCS7.getSubjectFields(cert).getField("CN"));
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS Z");
		layer2Text.append(" em " + format.format(today.getTime()));
		
		signatureAppearance.setLayer2Text(layer2Text.toString());
		Font fonte = new Font();
		fonte.setColor(BaseColor.GRAY);
		fonte.setSize(6F);
		
		signatureAppearance.setLayer2Font(fonte);
		signatureAppearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
		signatureAppearance.setVisibleSignature(new Rectangle(5, pdfOriginal.getPageSize(1).getHeight() - 40,
				pdfOriginal.getPageSize(1).getWidth() - 5, pdfOriginal.getPageSize(1)
						.getHeight() - 5), 1, null);
		
		try {
			pdfStamper.close();
		} catch (DocumentException e) {
			throw new IcpRuntimeException("Não foi possível criar assinatura", e);
		} catch (IOException e) {
			throw new IcpRuntimeException("Não foi possível criar assinatura", e);
		}
		pdfOriginal.close();
	}

	/**
	 * Constrói um objeto PKIXParameters, utilizado na validação do certificado.
	 * Indica que será utilizada a lista de revogação.
	 * Insere o trustedAnchors, com as autoridades certificadoras confiáveis.
	 * 
	 * @param crl
	 * @return
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchAlgorithmException
	 * @throws RaizNaoEncontradaException 
	 * @throws CRLInvalidaException 
	 */
	private PKIXParameters buildPkixParameters(List<X509Certificate> cadeia, Date date, boolean crlValidation) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, RaizNaoEncontradaException, CRLInvalidaException {
		
		Set<TrustAnchor> trustedAnchors = new HashSet<TrustAnchor>();
		// recupera a lista de ACs confiaveis
		List<X509Certificate> confiaveis = this.keyStoreService.getTrustedAnchors();
		
		if(confiaveis==null||confiaveis.size()==0){
			throw new RaizNaoEncontradaException("Nenhuma raiz confiável ICP-Brasil foi encontrada.");
		}
		
		for (X509Certificate c : confiaveis) {
			logger.debug(String.format("TrustAnchor [%s].",getCertString(c)));
			trustedAnchors.add(new TrustAnchor(c, null));
		}
		// cria o pkixParameters
		PKIXParameters pkixParameters = new PKIXParameters(trustedAnchors);
		if(crlValidation) {
			// validacao com lista de revogação
			logger.debug("RevocationEnabled");
			pkixParameters.setRevocationEnabled(true);
			
			// Recupera a lista de revogação dos certificados.
			List<CRL> crl = null;
			if (date == null) {
				crl = new LinkedList<CRL>(this.keyStoreService.getCrlList(cadeia));
			} else {
				crl = new LinkedList<CRL>(this.keyStoreService.getCrlList(cadeia, date));
			}

			// instancia um CertStore para armazenar a lista de revogação.
			List<CertStore> certStores = new ArrayList<CertStore>();
			certStores.add(CertStore.getInstance("Collection", new CollectionCertStoreParameters(crl)));
			// indica que a crl será utilizada para validar
			pkixParameters.setCertStores(certStores);
		} else {
			// validacao sem lista de revogação
			logger.debug("RevocationDisabled");
			pkixParameters.setRevocationEnabled(false);
		}
		
		// Indica a data de validação
		if (date != null) {
			pkixParameters.setDate(date);
		}
		
		pkixParameters.addCertPathChecker(new IcpBrasilCriticalExtensionsChecker());
		return pkixParameters;
	}

	/**
	 * Retorna uma string descritiva do certificado. Utilizada para
	 * logs e mensagens em exceptions.
	 * @param cert X509Certificate
	 * @return String
	 */
	private String getCertString(X509Certificate cert) {
		String certString = null;
		if (cert.getSubjectX500Principal() != null) {
			certString = cert.getSubjectX500Principal().getName();
		}
		return certString;
	}

	public KeyStoreService getKeyStoreService() {
		return keyStoreService;
	}

	public void setKeyStoreService(KeyStoreService keyStoreService) {
		this.keyStoreService = keyStoreService;
	}

	public boolean isAceitarAssinaturaCertificadoVencido() {
		return aceitarAssinaturaCertificadoVencido;
	}

	public void setAceitarAssinaturaCertificadoVencido(boolean aceitarAssinaturaCertificadoVencido) {
		this.aceitarAssinaturaCertificadoVencido = aceitarAssinaturaCertificadoVencido;
	}

	public boolean isAceitarUmaAssinaturaValida() {
		return aceitarUmaAssinaturaValida;
	}

	public void setAceitarUmaAssinaturaValida(boolean aceitarUmaAssinaturaValida) {
		this.aceitarUmaAssinaturaValida = aceitarUmaAssinaturaValida;
	}

	public boolean isAceitarAssinaturaSemCadeia() {
		return aceitarAssinaturaSemCadeia;
	}

	public void setAceitarAssinaturaSemCadeia(boolean aceitarAssinaturaSemCadeia) {
		this.aceitarAssinaturaSemCadeia = aceitarAssinaturaSemCadeia;
	}
}
