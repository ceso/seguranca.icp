package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.net.URL;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.security.auth.x500.X500Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;

import br.jus.stf.seguranca.icp.domain.AssinaturaDigital;
import br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora;
import br.jus.stf.seguranca.icp.domain.CentroDistribuicaoCRL;
import br.jus.stf.seguranca.icp.domain.CertificadoDigital;
import br.jus.stf.seguranca.icp.domain.Crl;
import br.jus.stf.seguranca.icp.domain.TipoCertificadoDigital;
import br.jus.stf.seguranca.icp.service.CadastroCertificadoService;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Service responsável por manter o cadastro de certificados digitais na base do STF.
 * 
 * @author Leandro.Oliveira
 */
@SuppressWarnings("deprecation")
public class CadastroCertificadoServiceImpl implements CadastroCertificadoService {

	private final Log logger = LogFactory.getLog(CadastroCertificadoServiceImpl.class);
	private JpaTemplate jpaTemplate;

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findAncorasConfiaveis()
	 */
	@SuppressWarnings("unchecked")
	public List<CertificadoDigital> findAncorasConfiaveis() {
		// recupera as raizes
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cd where cd.tipo in (?)",TipoCertificadoDigital.R);
		return certificados;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findIntermediarias()
	 */
	@SuppressWarnings("unchecked")
	public List<CertificadoDigital> findIntermediarias() {
		// recupera as autoridades certificadoras
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cd where cd.tipo in (?)",TipoCertificadoDigital.A);
		return certificados;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findEmitidosPor(br.jus.stf.seguranca.icp.domain.CertificadoDigital, br.jus.stf.seguranca.icp.domain.TipoCertificadoDigital)
	 */
	@SuppressWarnings("unchecked")
	public List<CertificadoDigital> findEmitidosPor(CertificadoDigital emissor, TipoCertificadoDigital tipoFilho) {
		// recupera as autoridades certificadoras
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cd where cd.tipo in (?) and cd.emissor.id = ? ",tipoFilho, emissor.getId());
		return certificados;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findCertificadoDigital(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<CertificadoDigital> findCertificadoDigital(String subjectDn) {
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cert where cert.descricao = ? order by cert.id asc", subjectDn);
		return certificados;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService.persistence.CertificadoDao#findCertificadoDigital(java.security.cert.X509Certificate)
	 */
	@SuppressWarnings("unchecked")
	public CertificadoDigital findCertificadoDigital(X509Certificate cert) {
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cert where cert.descricao = ? and cert.serial = ? and cert.emissor.descricao = ? order by cert.id asc",
				cert.getSubjectX500Principal().getName(), cert.getSerialNumber().toString(), cert.getIssuerX500Principal().getName());
		if (certificados.size() == 1) {
			return certificados.get(0);
		}
		if (certificados.size() > 1) {
			throw new IllegalStateException("Mais de um certificado encontrado.");
		}		
		return null;
	}
	
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findCertificadoDigital(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	public CertificadoDigital findCertificadoDigital(Long id) {
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cert where cert.id = ? ", id);
		if (certificados.size() == 1) {
			return certificados.get(0);
		}
		return null;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findCertificadosDigitaisUsuario(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	public List<CertificadoDigital> findCertificadosDigitaisUsuario(Long idUsuario) {
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cert where cert.seqUsuarioExterno = ? ", idUsuario);
		return certificados;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService.persistence.CertificadoDao#findAllAssinaturaDigital()
	 * @deprecated
	 */
	@SuppressWarnings( { "unchecked" })
	public List<AssinaturaDigital> findAllAssinaturaDigital() {
		List<AssinaturaDigital> usuarios = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.AssinaturaDigital ad order by ad.id asc");
		return usuarios;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findAllAutoridadeCertificadora()
	 */
	@SuppressWarnings("unchecked")
	public List<AutoridadeCertificadora> findAllAutoridadeCertificadora() {
		// recupera as raizes
		List<AutoridadeCertificadora> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora ac order by ac.id asc");
		return certificados;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService.persistence.CertificadoDao#findAutoridadeCertificadora(java.security.cert.X509Certificate)
	 * @deprecated
	 */
	@SuppressWarnings( { "unchecked" })
	public AutoridadeCertificadora findAutoridadeCertificadora(X509Certificate cert) {
		List<AutoridadeCertificadora> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora ac where ac.alias = ? and ac.numeroSerie = ?", cert.getSubjectX500Principal().getName(),
				cert.getSerialNumber().toString());
		if (certificados.size() > 0) {
			return certificados.get(0);
		}
		return null;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService.persistence.CertificadoDao#findIssuer(java.security.cert.X509Certificate)
	 */
	@SuppressWarnings("unchecked")
	public X509Certificate findIssuer(X509Certificate cert) {
		List<CertificadoDigital> certificados = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CertificadoDigital cert where cert.descricao = ? ", cert.getIssuerX500Principal().getName());
		for (CertificadoDigital certificadoDigital : certificados) {
			try {
				X509Certificate x509 = certificadoDigital.getX509();
				if (CertUtil.isIssuer(cert, x509)) {
					return x509;
				}
			} catch (IOException e) {
				// continua tentando outros certificados
				logger.error("Erro ao validar issuer: "+e.getMessage(), e);
			}
		}
		List<AutoridadeCertificadora> autoridades = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.AutoridadeCertificadora ac where ac.alias = ?", cert.getIssuerX500Principal().getName());
		for (AutoridadeCertificadora autoridadeCertificadora : autoridades) {
			try {
				X509Certificate x509 = autoridadeCertificadora.getX509();
				if (CertUtil.isIssuer(cert, x509)) {
					return x509;
				}
			} catch (CertificateException e) {
				// continua tentando outros certificados
				logger.error("Erro ao validar issuer: "+e.getMessage(), e);
			}
		}
		return null;
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#saveOrUpdate(br.jus.stf.seguranca.icp.domain.CertificadoDigital)
	 */
	public void saveOrUpdate(CertificadoDigital cert) {
		if (cert.getTipo().equals(TipoCertificadoDigital.R) || cert.getEmissor() == null || cert.equals(cert.getEmissor())) {
			// salva o certificado
			doSaveOrUpdate(cert);
			return;
		}
		// salva o emissor
		saveOrUpdate(cert.getEmissor());
		//salva o certificado
		doSaveOrUpdate(cert);
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#convertChain(java.util.List)
	 */
	public CertificadoDigital convertChain(List<X509Certificate> chainCerts) throws CertificateEncodingException, IOException {
		
		// Converte para array list para otimizar a manipulação
		ArrayList<X509Certificate> chain = new ArrayList<X509Certificate>(chainCerts);
		// Contador de controle
		int controle = 0;
		// Recupera o último da lista
		X509Certificate ultimoLista = chain.get(chain.size()-1);
		// Enquanto o último não for raiz, completará, até o limete de rodadas de controle
		while (!CertUtil.isRaiz(ultimoLista) || (controle++ < 10)) {
			X509Certificate issuerUltimo = this.findIssuer(ultimoLista);
			if (issuerUltimo == null) {
				break;
			}
			if (!chain.contains(issuerUltimo)) {
				chain.add(issuerUltimo);
			} else {
				break;
			}
			ultimoLista = chain.get(chain.size() - 1);
		}
		
		// Cria um array para conter a saída
		CertificadoDigital[] saida = new CertificadoDigital[chain.size()];
		
		for (int i = 0; i < saida.length; i++) {
			// recupera algum cadastro já existente
			CertificadoDigital cert = findCertificadoDigital(chain.get(i));
			if (cert == null) {
				cert = criarCertificadoDigital(chain.get(i));
			}
			// salva o certificado no array
			saida[i] = cert;			
			if (i > 0) {
				// atualiza o emissor do certificado anterior
				saida[i - 1].setEmissor(cert);
				// atualiza as cdps deste certificado com base no anterior
				List<URL> cdps = CertUtil.getCdpFromX509Cert(saida[i -1].getX509());
				for (URL url : cdps) {
					cert.addCentroDistribuicao(url);	
				}
			}
		}
		return saida[0];
	}
	
	/**
	 * Cria um CertificadoDigital com base em um X509Certificate.
	 * 
	 * @param x509
	 *            X509Certificate
	 * @return CertificadoDigital
	 * @throws IOException
	 * @throws CertificateEncodingException
	 */
	private CertificadoDigital criarCertificadoDigital(X509Certificate x509) throws IOException, CertificateEncodingException {
		CertificadoDigital certificadoDigital = new CertificadoDigital();
		certificadoDigital.setDescricao(x509.getSubjectX500Principal().getName());
		certificadoDigital.setSerial(x509.getSerialNumber().toString());
		certificadoDigital.setInicioValidade(x509.getNotBefore());
		certificadoDigital.setFimValidade(x509.getNotAfter());
		certificadoDigital.setCertificado(CertUtil.toPem(x509));
		if (CertUtil.isRaiz(x509)) {
			// indica o tipo raiz para o certificado
			certificadoDigital.setTipo(TipoCertificadoDigital.R);
			certificadoDigital.setEmissor(certificadoDigital);
		} else if (CertUtil.isFolhaIcpBrasil(x509)) {
			// indica o tipo final (ou folha) para o certificado
			certificadoDigital.setTipo(TipoCertificadoDigital.F);
		} else {
			// indica o tipo autoridade certificadora para o certificado
			certificadoDigital.setTipo(TipoCertificadoDigital.A);
		}
		return certificadoDigital;
	}

	/**
	 * Insere ou atualiza o certificado.
	 * @param cert CertificadoDigital
	 */
	private void doSaveOrUpdate(CertificadoDigital cert){
		if (cert.getId() == null) {
			getJpaTemplate().persist(cert);
		} else {
			getJpaTemplate().merge(cert);
		}
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#saveOrUpdate(br.jus.stf.seguranca.icp.domain.Crl)
	 */
	public void saveOrUpdate(Crl crl) {
		if (crl.getId() == null) {
			getJpaTemplate().persist(crl);
		} else {
			getJpaTemplate().merge(crl);
		}
	}

	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService.persistence.CertificadoDao#findChain(java.security.cert.X509Certificate)
	 */
	public List<X509Certificate> findChain(X509Certificate usr) throws CadeiaException {
		
		// se o certificado for null, exception
		if (usr == null) {
			throw new NullPointerException("É necessário informar um certificado para recuperar a cadeia.");
		}
		List<X509Certificate> chain = new LinkedList<X509Certificate>();
		X509Certificate currentCert = usr;
		// controla apenas para nao entrar em loop infinito (ocupando 100% de cpu)
		int controle = 0;
		while (true) {
			logger.debug(String.format("Chain Array: [%s]", CertUtil.getCertString(currentCert)));
			chain.add(currentCert);
			// se for raiz, pára o loop
			if (CertUtil.isRaiz(currentCert)) {
				break;
			}
			X500Principal issuerPrincipal = currentCert.getIssuerX500Principal();
			// se não houver issuer, (provavelmente um erro no certificado), pára o loop
			if (issuerPrincipal == null || issuerPrincipal.getName() == null || issuerPrincipal.getName().trim().length() == 0) {
				throw new CadeiaException("O issuer do certificado não existe.");
			}
			// recupera o issuer
			X509Certificate issuer = findIssuer(currentCert);
			if (issuer == null) {
				throw new CadeiaException("Não foi possível recuperar na base de cadastro do STF a cadeia completa. Certificado procurado: " + currentCert.getIssuerX500Principal().getName());
			}
			// atualiza o certificado corrente
			currentCert = issuer;
			if (controle++ == 70) {
				throw new CadeiaException("Trava contra loop infinito, o código que recupera a cadeia completou 70 chamadas. ");
			}
		}
		return chain;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findCentroDistribuicao(java.net.URL)
	 */
	@SuppressWarnings("unchecked")
	public CentroDistribuicaoCRL findCentroDistribuicao(URL url, CertificadoDigital issuer) {
		List<CentroDistribuicaoCRL> cdps = getJpaTemplate().find("from br.jus.stf.seguranca.icp.domain.CentroDistribuicaoCRL cdp where cdp.url = ? and cdp.certificado.id = ?", url.toExternalForm(), issuer.getId());
		if(cdps.size()==1){
			return cdps.get(0);
		}
		if (cdps.size() > 1) {
			throw new IllegalStateException("Estado invalido do banco de dados. Mais de uma CDP foi encontrada com a mesma url: " + url);
		}
		return null;
	}
	
	/**
	 * @see br.jus.stf.seguranca.icp.service.CadastroCertificadoService#findLatestCrl(br.jus.stf.seguranca.icp.domain.CertificadoDigital)
	 */
	@SuppressWarnings("unchecked")
	public Crl findLatestCrl(final CertificadoDigital issuer) {
		Crl crl = (Crl) getJpaTemplate().execute(new JpaCallback() {
			public Object doInJpa(EntityManager em) throws PersistenceException {
				Query query = em.createQuery("from br.jus.stf.seguranca.icp.domain.Crl crl where crl.certificado.id = ? order by crl.id desc");
				query.setParameter(1, issuer.getId());
				query.setMaxResults(5);
				List<Crl> crls = query.getResultList();
				if (crls.size() == 0) {
					return null;
				}
				return crls.get(0);
			}
		});
		return crl;
	}

	public JpaTemplate getJpaTemplate() {
		return jpaTemplate;
	}

	public void setJpaTemplate(JpaTemplate jpaTemplate) {
		this.jpaTemplate = jpaTemplate;
	}
}
