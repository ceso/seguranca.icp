package br.jus.stf.seguranca.icp.util;

public class SubjectAlternativeNameICPBrasil {
	private String cpf_cnpj;
	private String codigoOAB;
	private String ufOAB;
	private String nomeTitular;
	private String email;
	
	//Retrocompatibilidade
	public String getCpf() {
		return cpf_cnpj;
	}

	//Retrocompatibilidade
	public void setCpf(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}

	public String getCpfCnpj() {
		return cpf_cnpj;
	}


	public void setCpfCnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}


	public String getCodigoOAB() {
		return codigoOAB;
	}


	public void setCodigoOAB(String codigoOAB) {
		this.codigoOAB = codigoOAB;
	}


	public String getUfOAB() {
		return ufOAB;
	}


	public void setUfOAB(String ufOAB) {
		this.ufOAB = ufOAB;
	}


	public String getNomeTitular() {
		return nomeTitular;
	}


	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

}
