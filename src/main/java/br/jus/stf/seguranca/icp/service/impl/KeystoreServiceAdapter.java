package br.jus.stf.seguranca.icp.service.impl;

import java.security.PrivateKey;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;

/**
 * Adapter. 
 * @author Leandro.Oliveira
 */
public abstract class KeystoreServiceAdapter implements KeyStoreService {
	
	private KeyStoreService keyStoreService;
	
	public KeystoreServiceAdapter(KeyStoreService keyStoreService){
		this.keyStoreService = keyStoreService;
	}

	public List<String> aliases() {
		return this.keyStoreService.aliases();
	}

	public X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException {
		return this.keyStoreService.getCertificateByAlias(alias);
	}

	public X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException {
		return this.keyStoreService.getCertificateBySubjectName(subjectDn);
	}

	public List<X509Certificate> getCertificateChain(X509Certificate cert) throws CertificadoInvalidoException, CadeiaException {
		return this.keyStoreService.getCertificateChain(cert);
	}

	public X509CRL getCrl(X509Certificate cert) throws CRLInvalidaException {
		return this.keyStoreService.getCrl(cert);
	}

	public X509CRL getCrl(X509Certificate cert, Date date) throws CRLInvalidaException {
		return this.keyStoreService.getCrl(cert, date);
	}

	public List<X509CRL> getCrlList(List<X509Certificate> chain) throws CRLInvalidaException {
		return this.keyStoreService.getCrlList(chain);
	}

	public List<X509CRL> getCrlList(List<X509Certificate> chain, Date data) throws CRLInvalidaException {
		return this.keyStoreService.getCrlList(chain, data);
	}

	public PrivateKey getPrivateKey(String alias, String password) {
		return this.keyStoreService.getPrivateKey(alias, password);
	}

	public List<X509Certificate> getTrustedAnchors() {
		return this.keyStoreService.getTrustedAnchors();
	}

}
