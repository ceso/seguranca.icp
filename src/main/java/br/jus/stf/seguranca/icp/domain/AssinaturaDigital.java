package br.jus.stf.seguranca.icp.domain;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

/**
 * Representa os certificados de entidade final emitidos para usuÃ¡rios externos
 * de sistemas do STF.
 * Classe depreciada, a classe CertificadoDigital deverÃ¡ substituir esta.
 * 
 * 
 * @deprecated
 * @author Leandro.Oliveira
 * 
 */
@Entity
@Table(schema = "CORP", name = "ASSINATURA_DIGITAL")
public class AssinaturaDigital implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private byte[] certificadoDigital;
	private byte[] seloCronologico;
	private String serial;
	private AutoridadeCertificadora autoridadeCertificadora;
	private Date cancelamento;
	private Boolean ativo;
	private CertificadoDigital emissor;
	private Long seqUsuarioExterno;

	@Id
	@SequenceGenerator(name = "ASSINATURA_DIGITAL_ID_GENERATOR", sequenceName = "CORP.SEQ_ASSINATURA_DIGITAL" , allocationSize = 0)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSINATURA_DIGITAL_ID_GENERATOR")
	@Column(name = "SEQ_ASSINATURA_DIGITAL", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Lob()
	@Column(name = "BIN_ASSINATURA_DIGITAL")
	public byte[] getCertificadoDigital() {
		return certificadoDigital;
	}

	public void setCertificadoDigital(byte[] certificadoDigital) {
		this.certificadoDigital = certificadoDigital;
	}

	@Lob()
	@Column(name = "BIN_SELO_CRONOLOGICO")
	public byte[] getSeloCronologico() {
		return seloCronologico;
	}

	public void setSeloCronologico(byte[] seloCronologico) {
		this.seloCronologico = seloCronologico;
	}

	@Column(name = "COD_SERIAL")
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	@ManyToOne(cascade={CascadeType.PERSIST}, fetch=FetchType.LAZY)
	@JoinColumn(name="SEQ_AUTORIDADE_CERTIFICADORA")
	public AutoridadeCertificadora getAutoridadeCertificadora() {
		return autoridadeCertificadora;
	}

	public void setAutoridadeCertificadora(AutoridadeCertificadora autoridadeCertificadora) {
		this.autoridadeCertificadora = autoridadeCertificadora;
	}

	@Column(name = "DAT_CANCELAMENTO")
	public Date getCancelamento() {
		return cancelamento;
	}

	public void setCancelamento(Date cancelamento) {
		this.cancelamento = cancelamento;
	}

	@Type(type = "br.jus.stf.seguranca.icp.domain.FlagSimNaoType")
	@Column(name = "FLG_ATIVO")
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@ManyToOne(cascade={CascadeType.PERSIST}, fetch=FetchType.LAZY)
	@JoinColumn(name="SEQ_CERTIFICADO_DIGITAL")
	public CertificadoDigital getEmissor() {
		return emissor;
	}

	public void setEmissor(CertificadoDigital emissor) {
		this.emissor = emissor;
	}
	
	@Column(name = "SEQ_USUARIO_EXTERNO", length = 60)
	public Long getSeqUsuarioExterno() {
		return seqUsuarioExterno;
	}

	public void setSeqUsuarioExterno(Long seqUsuarioExterno) {
		this.seqUsuarioExterno = seqUsuarioExterno;
	}
	
	@Transient
	public X509Certificate getX509() throws CertificateException {
		if(getCertificadoDigital()==null){
			return null;
		}
		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(getCertificadoDigital()));
	}
}
