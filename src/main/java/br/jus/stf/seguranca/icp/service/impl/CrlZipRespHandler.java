package br.jus.stf.seguranca.icp.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;

/**
 * Recupera da requisição a lista de CRLs. 
 * @author Leandro.Oliveira
 */
public class CrlZipRespHandler implements ResponseHandler<LinkedHashMap<X509Certificate,X509CRL>>{

	private final Log logger = LogFactory.getLog(KeyStoreServiceHttpZipImpl.class);
	private LinkedHashMap<String, X509Certificate> cas;
	public CrlZipRespHandler(LinkedHashMap<String, X509Certificate> cas){
		this.cas = cas;
	}
	public LinkedHashMap<X509Certificate, X509CRL> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		StatusLine status = response.getStatusLine();
		// se status diferente de 200
		if (status.getStatusCode() != HttpStatus.SC_OK) {
			// gera erro
			throw new HttpResponseException(status.getStatusCode(), response.getStatusLine().getReasonPhrase());
		} else {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream entityContent = null;
				try {
					entityContent = entity.getContent();
					ZipInputStream zipInputStream = new ZipInputStream(entityContent);
					// monta a lista de CRLs
					return populateCrl(zipInputStream);
				} finally {
					if (entityContent != null) {
						entityContent.close();
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Monta a lista de CAs a partir de um zip.
	 * 
	 * @param zip
	 *            ZipFile
	 * @return LinkedHashMap<String, X509Certificate>
	 * @throws CertificateException
	 * @throws IOException
	 * @throws CRLException 
	 */
	private LinkedHashMap<X509Certificate,X509CRL> populateCrl(ZipInputStream zipIn) throws IOException{
		CertificateFactory factory;
		try {
			factory = CertificateFactory.getInstance("X.509");
		} catch (CertificateException e) {
			logger.error("Erro ao popular lista de CRLs.", e);
			throw new HttpResponseException(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
		}
		LinkedHashMap<X509Certificate,X509CRL> temp = new LinkedHashMap<X509Certificate, X509CRL>();
		ZipEntry ze;
		while ((ze = zipIn.getNextEntry()) != null) {
			logger.debug(String.format("CRL recebida: %s tamanho %d.", ze.getName(), ze.getSize()));
			X509CRL crl;
			try {
				crl = (X509CRL) factory.generateCRL(zipIn);
				temp.put(cas.get(crl.getIssuerX500Principal().getName()), crl);
			} catch (CRLException e) {
				logger.error("Erro ao processar o item: "+ze.getName()+"; erro: "+e.getMessage(),e);
			}
		}
		return temp;
	}
}
