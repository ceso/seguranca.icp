package br.jus.stf.seguranca.icp.util;

import java.io.IOException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.x509.X509Extension;

/**
 * Classe responsÃ¡vel para permitir a validaÃ§Ã£o de certificados ICP-Brasil que possuem itens para o campo
 * uso estendido da chave (extendedKeyUsageOid - 2.5.29.37).
 * 
 * @author Leandro.Oliveira
 */
public class IcpBrasilCriticalExtensionsChecker extends PKIXCertPathChecker {
	
	/** Extended key usage - one or more purposes for which the certified public key may be used. */
	private static final String extendedKeyUsageOid = "2.5.29.37";
	private static final Set<String> supportedExtensions = new LinkedHashSet<String>();
	
    public static String id_kp_serverAuth = "1.3.6.1.5.5.7.3.1";
    public static String id_kp_clientAuth = "1.3.6.1.5.5.7.3.2";
    public static String id_kp_codeSigning = "1.3.6.1.5.5.7.3.3";
    public static String id_kp_emailProtection = "1.3.6.1.5.5.7.3.4";
    public static String id_kp_ipsecEndSystem = "1.3.6.1.5.5.7.3.5";
    public static String id_kp_ipsecTunnel = "1.3.6.1.5.5.7.3.6";
    public static String id_kp_ipsecUser = "1.3.6.1.5.5.7.3.7";
    public static String id_kp_timeStamping = "1.3.6.1.5.5.7.3.8";
    public static String OCSPSigning = "1.3.6.1.5.5.7.3.9";
    public static String id_ms_smart_card_logon = "1.3.6.1.4.1.311.20.2.2";
	
    /**
     * Construtor.
     */
	public IcpBrasilCriticalExtensionsChecker() {
		supportedExtensions.add(extendedKeyUsageOid);
	}

	@Override
	public void init(boolean forward) throws CertPathValidatorException {
	}
	
	/**
	 * Verifica se o certificado possui algum item abaixo de extended key usage como crÃ­tico.
	 * Se sim, e se o item for email protection, client auth ou microsoft smart card logon, permite a validaÃ§Ã£o do certificado.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void check(Certificate cert, Collection<String> unresolvedCritExts) throws CertPathValidatorException {
		boolean contains = unresolvedCritExts.contains(extendedKeyUsageOid);
		if (contains) {
			try {
				X509Certificate x509 = (X509Certificate) cert;
				byte[] extendedKeyUsage = x509.getExtensionValue(X509Extension.extendedKeyUsage.getId());
				ASN1OctetString octetString = (ASN1OctetString) getDerObject(extendedKeyUsage);
				ASN1Sequence asn1Sequence = (ASN1Sequence) getDerObject(octetString.getOctets());
				Enumeration enumeration = asn1Sequence.getObjects();
				while (enumeration.hasMoreElements()) {
					ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) enumeration.nextElement();
					// Verifica se o oid estÃ¡ dentre os permitidos
					if( !(oid.getId().equals(id_kp_emailProtection) || oid.getId().equals(id_kp_clientAuth) || oid.getId().equals(id_ms_smart_card_logon)) ) {
						throw new CertPathValidatorException("Certificado recusado por possuir o elemento crÃ­tico de oid " + oid.getId()
								+ " nÃ£o previsto no campo extendedKeyUsageOid (2.5.29.37) ");
					}
				}
			} catch (IOException e) {
				throw new CertPathValidatorException(e);
			}
			unresolvedCritExts.remove(extendedKeyUsageOid);
		}
	}
	
	/**
	 * Trata um array retornando o DERObject correspondente.
	 * 
	 * @param data
	 *            byte[]
	 * @return DERObject
	 * @throws IOException
	 */
	private static DERObject getDerObject(byte data[]) throws IOException {
		ASN1InputStream stream = new ASN1InputStream(data);
		return stream.readObject();
	}

	@Override
	public Set<String> getSupportedExtensions() {
		return supportedExtensions;
	}

	@Override
	public boolean isForwardCheckingSupported() {
		return true;
	}

}
