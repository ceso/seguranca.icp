package br.jus.stf.seguranca.icp.service.exception;

/**
 * Identifica erros em documentos.
 * 
 * @author Leandro.Oliveira
 */
public class DocumentoInvalidoException extends Exception {
	private static final long serialVersionUID = 1L;
	/**mensagem que deve ser apresentada ao usuario. */
	private String mensagem;
	/**mensagem detalhada do erro, com informacoes retiradas da exception de causa raiz */
	private String mensagemDetalhada;
	
	/**
	 * 
	 * @param mensagem
	 * @param mensagemDetalhada
	 */
	public DocumentoInvalidoException(String mensagem, String mensagemDetalhada) {
		this.mensagem = mensagem;
		this.mensagemDetalhada = mensagemDetalhada;
	}
	
	/**
	 * 
	 * @param mensagem
	 */
	public DocumentoInvalidoException(String mensagem){
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	public String getMensagemDetalhada() {
		return mensagemDetalhada;
	}

	@Override
	public String getMessage() {
		return getMensagemDetalhada();
	}
}
