package br.jus.stf.seguranca.icp.service;

import org.bouncycastle.tsp.TimeStampToken;

import br.jus.stf.seguranca.icp.service.exception.CarimbadoraException;

/**
 * Centraliza o acesso a carimbadoras de tempo.
 * 
 * 
 * @author andre.pereira
 */
public interface CarimbadoraService {
	
	/**
	 * Gera SHA1 para <code>data</code>.
	 * Aplica o carimbo para SHA1(data).
	 * 
	 * @param data String
	 * @return TimeStampToken
	 * @throws CarimbadoraException
	 */
	TimeStampToken carimbarDados(String data) throws CarimbadoraException;
	
	/**
	 * Aplica o carimbo para digesto.
	 * @param digesto
	 * @return
	 * @throws CarimbadoraException
	 */
	TimeStampToken carimbarDigesto(byte[] digesto) throws CarimbadoraException;
}
