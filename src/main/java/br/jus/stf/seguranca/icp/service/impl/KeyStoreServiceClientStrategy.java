package br.jus.stf.seguranca.icp.service.impl;

import java.security.PrivateKey;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.jus.stf.seguranca.icp.service.KeyStoreService;
import br.jus.stf.seguranca.icp.service.exception.CRLInvalidaException;
import br.jus.stf.seguranca.icp.service.exception.CadeiaException;
import br.jus.stf.seguranca.icp.service.exception.CertificadoInvalidoException;

/**
 * Executa o seguinte procedimento:
 * Recupera aliases de certificados pessoais - Windows-My.
 * Procura certificados, monta cadeias, Raízes e CRLs procurando primeiramente na máquina do usuário.
 * Caso alguma operação falhe, procura em um Zip por Http por certificados.
 * 
 * @author Leandro.Oliveira
 */
public class KeyStoreServiceClientStrategy implements KeyStoreService {
	private final Log logger = LogFactory.getLog(KeyStoreServiceClientStrategy.class);
	private KeyStoreServiceHttpZipImpl zipService;
	private KeyStoreServiceSunMsCapiImpl msCapiService;
	/** Se true, tenta baixar localmente (na máquina do usuario) as CRLs e a Cadeia antes de buscar em zip. **/
	private boolean baixarLocalmente = true;

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#aliases()
	 */
	public List<String> aliases() {
		// Sempre localmente
		return msCapiService.windowsMyAliases();
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateByAlias(java.lang.String)
	 */
	public X509Certificate getCertificateByAlias(String alias) throws CertificadoInvalidoException {
		// Sempre localmente
		return msCapiService.getCertificateByAlias(alias);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateBySubjectName(java.lang.String)
	 */
	public X509Certificate getCertificateBySubjectName(String subjectDn) throws CertificadoInvalidoException {
		// Tenta baixar localmente antes de buscar no zip
		if(baixarLocalmente) {
			try {
				return msCapiService.getCertificateBySubjectName(subjectDn);
			} catch (UnsupportedOperationException e) {
				logger.info(e.getMessage());
			}
		}
		logger.info("Recuperando certificado por zip.");
		return zipService.getCertificateBySubjectName(subjectDn);
	}

	/**
	 * @throws CadeiaException 
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCertificateChain(java.security.cert.X509Certificate)
	 */
	public List<X509Certificate> getCertificateChain(X509Certificate cert) throws CertificadoInvalidoException, CadeiaException {
		// Tenta baixar localmente antes de buscar no zip
		if(baixarLocalmente){
			try {
				return msCapiService.getCertificateChain(cert);
			} catch (UnsupportedOperationException e) {
				logger.info("Não foi posível montar a cadeia através do SunMsCapi. Erro: "+e.getMessage(),e);
				logger.debug(e);
			}
		}
		logger.info("Recuperando cadeia por zip.");
		return zipService.getCertificateChain(cert);
	}
	

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate)
	 */
	public X509CRL getCrl(X509Certificate cert) throws CRLInvalidaException {
		return getCrl(cert, new Date());
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrl(java.security.cert.X509Certificate)
	 */
	public X509CRL getCrl(X509Certificate cert, Date date) throws CRLInvalidaException {
		// Tenta baixar localmente antes de buscar no zip
		if(baixarLocalmente) {
			try {
				return msCapiService.getCrl(cert, date);
			} catch (UnsupportedOperationException e) {
				logger.info("Não foi posível recuperar CRL através do SunMsCapi. Erro: "+e.getMessage(),e);
				logger.debug(e);
			}
		}
		logger.info("Recuperando certificado por zip.");
		return zipService.getCrl(cert, date);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrlList(java.util.List, java.util.Date)
	 */
	public List<X509CRL> getCrlList(List<X509Certificate> chain, Date data) throws CRLInvalidaException {
		// Tenta baixar localmente antes de buscar no zip
		if(baixarLocalmente){
			try {
				return msCapiService.getCrlList(chain, data);
			} catch (UnsupportedOperationException e) {
				logger.info("Não foi posível recuperar CRLs através do SunMsCapi. Erro: "+e.getMessage(),e);
				logger.debug(e);
			}
		}
		logger.info("Recuperando CRLs por zip.");
		return zipService.getCrlList(chain, data);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getCrlList(java.util.List)
	 */
	public List<X509CRL> getCrlList(List<X509Certificate> chain) throws CRLInvalidaException {
		return getCrlList(chain, new Date());
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getPrivateKey(java.lang.String, java.lang.String)
	 */
	public PrivateKey getPrivateKey(String alias, String password) {
		// Sempre localmente, por um provider adequado
		return msCapiService.getPrivateKey(alias, password);
	}

	/**
	 * @see br.jus.stf.seguranca.icp.service.KeyStoreService#getTrustedAnchors()
	 */
	public List<X509Certificate> getTrustedAnchors() {
		// Os trusted anchors sao identificados ao montar a cadeia localmente
		// se nao estiver baixando a cadeia localmente, os trusted anchors seriam null.
		if(baixarLocalmente){
			try {
				return msCapiService.getTrustedAnchors();
			} catch (UnsupportedOperationException e) {
				logger.info("Não foi possível recuperar o trusted anchors pela máquina do usuário");
				logger.debug(e);
			}
		}
		logger.info("Recuperando trusted anchors por zip.");
		return zipService.getTrustedAnchors();
	}

	public KeyStoreServiceHttpZipImpl getZipService() {
		return zipService;
	}

	public void setZipService(KeyStoreServiceHttpZipImpl zipService) {
		this.zipService = zipService;
	}

	public KeyStoreServiceSunMsCapiImpl getMsCapiService() {
		return msCapiService;
	}

	public void setMsCapiService(KeyStoreServiceSunMsCapiImpl msCapiService) {
		this.msCapiService = msCapiService;
	}

	public boolean isBaixarLocalmente() {
		return baixarLocalmente;
	}

	public void setBaixarLocalmente(boolean baixarLocalmente) {
		this.baixarLocalmente = baixarLocalmente;
	}

}
