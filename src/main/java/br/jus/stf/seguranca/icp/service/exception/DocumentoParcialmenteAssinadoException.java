package br.jus.stf.seguranca.icp.service.exception;

/**
 * Marca um erro de validacao de assinaturas em que a assinatura validada nao compreende todo o documento.
 * 
 * @author Leandro.Oliveira
 */
public class DocumentoParcialmenteAssinadoException extends AssinaturaInvalidaException {

	private static final long serialVersionUID = 1L;

	/**
	 * Construtor.
	 * @param mensagem
	 * @param mensagemDetalhada
	 */
	public DocumentoParcialmenteAssinadoException(String mensagem, String mensagemDetalhada) {
		super(mensagem, mensagemDetalhada);
	}

	/**
	 * Construtor.
	 * @param mensagem
	 */
	public DocumentoParcialmenteAssinadoException(String mensagem) {
		super(mensagem);
	}
}
