package br.jus.stf.seguranca.icp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * Centro de Distribuição de CRLs de uma determinada Autoridade Intermediária ou da própria raiz.
 * Pode ser desativado.
 * 
 * @author Leandro.Oliveira
 *
 */
@Entity
@Table(schema = "CORP", name = "CENTRO_DISTRIBUICAO_LCR")
public class CentroDistribuicaoCRL implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private CertificadoDigital certificado;
	private String url;
	private Boolean ativo;

	@Id
	@Column(name = "SEQ_CENTRO_DISTRIBUICAO_LCR")
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sequence", sequenceName = "CORP.SEQ_CENTRO_DISTRIBUICAO_LCR", allocationSize=0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DSC_ENDERECO_URL", length = 350)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "SEQ_CERTIFICADO_DIGITAL", referencedColumnName = "SEQ_CERTIFICADO_DIGITAL")
	public CertificadoDigital getCertificado() {
		return certificado;
	}

	public void setCertificado(CertificadoDigital certificado) {
		this.certificado = certificado;
	}

	@Type(type = "br.jus.stf.seguranca.icp.domain.FlagSimNaoType")
	@Column(name = "FLG_ATIVO")
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
