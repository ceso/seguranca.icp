package br.jus.stf.seguranca.icp.service.impl.crlmanager;

import java.io.IOException;
import java.net.URL;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SchemeRegistryFactory;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;

import br.jus.stf.seguranca.icp.service.impl.RequestRetryHandler;
import br.jus.stf.seguranca.icp.util.CertUtil;

/**
 * Recupera utilizando HttpClient um conjunto de CRLs.
 * Dispara vÃ¡rias requisiÃ§Ãµes de maneira assincrona.
 * 
 *  
 * @author Leandro.Oliveira
 */
public class CrlManager {
	private LinkedHashMap<X509Certificate, CrlDownloadManager> certificados = new LinkedHashMap<X509Certificate, CrlDownloadManager>();
	private HttpClient httpClient;
	private ScheduledExecutorService executor;
	/** Timeout de conexÃ£o padrÃ£o de 7s*/
	private int connectionTimeout = 7000;
	/** Timeout de intervalo entre pacotes de 7s*/
	private int waitForPacketTimeout = 7000;
	/** Tempo padrÃ£o de espera pelo download. MÃ©todo waitForCert. PadrÃ£o, 5 segundos.*/
	private long defaultCrlWaitTime = 5000;

	/**
	 * Cria o CrlManager.
	 */
	public CrlManager() {
		this(5);
	}
	
	/**
	 * Cria o CrlManager com um nÃºmero determinado de worker threads.
	 * 
	 * @param poolCount int
	 */
	public CrlManager(int poolCount) {
		initHttpClient();
		executor = Executors.newScheduledThreadPool(poolCount);
	}
	
	/**
	 * Cria um crl manager.
	 * @param poolCount int nÃºmero de threads.
	 * @param connectionTimeout int timeout de conexÃ£o millis.
	 * @param waitForPacketTimeout int timeout de pacote millis.
	 */
	public CrlManager(int poolCount, int connectionTimeout, int waitForPacketTimeout, long defaultCrlWaitTime){
		this.connectionTimeout = connectionTimeout;
		this.waitForPacketTimeout = waitForPacketTimeout;
		initHttpClient();
		executor = Executors.newScheduledThreadPool(poolCount);
		this.defaultCrlWaitTime = defaultCrlWaitTime;
	}
	
	/**
	 * Cria o objeto HttpClient
	 */
	private void initHttpClient() {
		ThreadSafeClientConnManager connectionManager = new ThreadSafeClientConnManager(SchemeRegistryFactory.createDefault(),10000,TimeUnit.MILLISECONDS);
		// cria o http client
		DefaultHttpClient tempClient = new DefaultHttpClient(connectionManager);
		// Tempo para abrir uma conexÃ£o: 600ms.
		tempClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, this.connectionTimeout);
		// Timeout para intervalo de pacotes.
		tempClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, this.waitForPacketTimeout);
		// Some tuning that is not required for bit tests.
		tempClient.getParams().setParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);
		tempClient.getParams().setParameter(CoreConnectionPNames.TCP_NODELAY, true);
		tempClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		
		// realizar 3 tentativas
		tempClient.setHttpRequestRetryHandler(new RequestRetryHandler());
		httpClient = tempClient;
	}

	/**
	 * Adiciona um certificado ao conjunto de certificados cujos CDPs serÃ£o pesquisados.
	 * @param cert X509Certificate
	 * @throws IOException
	 */
	public void addCertificado(X509Certificate cert, List<URL> urls) throws IOException {
		// Indica cdpsFromCertificate com falso, influencia no tipo de CRL que pode ser recuperada.
		addCertificado(cert, urls, false);
	}
	
	/**
	 * Adiciona um certificado ao conjunto de certificados cujos CDPs serÃ£o pesquisados.
	 * @param cert
	 * @throws IOException
	 */
	public void addCertificado(X509Certificate cert) throws IOException {
		// Indica cdpsFromCertificate com true, influencia no tipo de CRL que pode ser recuperada.
		this.addCertificado(cert, CertUtil.getCdpFromX509Cert(cert), true);
	}
	
	/**
	 * Adiciona um certificado e espera pelo download.
	 * @param cert
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void addCertificadoAndWait(X509Certificate cert, List<URL> urls) throws IOException, InterruptedException {
		// Indica cdpsFromCertificate com falso, influencia no tipo de CRL que pode ser recuperada.
		CrlDownloadManager dm = addCertificado(cert, urls, false);
		dm.waitForCrl();
	}
	
	/**
	 * Adiciona um certificado e espera pelo download.
	 * @param cert
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void addCertificadoAndWait(X509Certificate cert) throws IOException, InterruptedException {
		// Indica cdpsFromCertificate com true, influencia no tipo de CRL que pode ser recuperada.
		CrlDownloadManager dm = this.addCertificado(cert, CertUtil.getCdpFromX509Cert(cert), true);
		dm.waitForCrl();
	}
	
	/**
	 * Adiciona um certificado ao conjunto de certificados cujos CDPs serÃ£o pesquisados.
	 * 
	 * @param cert X509Certificate - que necessita da CRL. Ou o prÃ³prio emissor da CRL, ou um certificado emitido por este.
	 * @param urls List<URL> - Centros de distribuiÃ§Ã£o de CRLs.
	 * @param cdpsFromCertificate boolean - indica que as CDPs foram retiradas do <code>cert</code>
	 * @throws IOException
	 */
	private synchronized CrlDownloadManager addCertificado(X509Certificate cert, List<URL> urls, boolean cdpsFromCertificate) throws IOException {
		CrlDownloadManager dm;
		if (!certificados.containsKey(cert)) {
			dm = new CrlDownloadManager(cert, httpClient, executor, cdpsFromCertificate, defaultCrlWaitTime);
			certificados.put(cert, dm);
		} else {
			dm = certificados.get(cert);
		}
		for (URL url : urls) {
			dm.addUrl(url);
		}
		return dm;
	}
	
	/**
	 * Executor utilizado pelo gerenciador de download de CRL.
	 * @return ScheduledExecutorService
	 */
	public ScheduledExecutorService getExecutor() {
		return executor;
	}
	
	/**
	 * Recupera a CRL para um determinado Issuer.
	 * @param issuer
	 * @return
	 */
	public synchronized X509CRL getCrlByIssuer(X509Certificate issuer) {
		Set<Entry<X509Certificate, CrlDownloadManager>> entries = certificados.entrySet();
		// procura em todos os downloads
		for (Entry<X509Certificate, CrlDownloadManager> entry : entries) {
			X509CRL crl = entry.getValue().getCrlDto().getCrl();
			if(crl==null){
				continue;
			}
			// retorna a crl que corresponder ao issuer
			if (crl.getIssuerX500Principal().equals(issuer.getSubjectX500Principal())) {
				return crl;
			}
		}
		return null;
	}
	
	/**
	 * Recupera a CRL para um determinado Certificado.
	 * @param cert
	 * @return
	 */
	public synchronized X509CRL getCrlByCert(X509Certificate cert) {
		CrlDownloadManager dm = certificados.get(cert);
		if(dm!=null){
			return dm.getCrlDto().getCrl();
		}
		return null;
	}
	
	/**
	 * Recupera a CRL com sua respectiva URL.
	 * @param cert X509Certificate
	 * @return CrlDto
	 */
	public synchronized CrlDto getCrlDtoByCert(X509Certificate cert) {
		CrlDownloadManager dm = certificados.get(cert);
		if(dm==null){
			throw new IllegalArgumentException(String.format("Certificado nao monitorado: %s",CertUtil.getCertString(cert)));
		}
		return dm.getCrlDto();
	}
}
